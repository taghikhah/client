// Libraries
import React, { Component } from "react";
import PropTypes from "prop-types";

// Child Components
import Actions from "../common/table/actions";
import Table from "../common/table";

class CategoriesTable extends Component {
  columns = [
    { path: "name_en", label: "Name (English)", isSortable: true },
    { path: "name_de", label: "Name (German)", isSortable: true },
    { path: "priority", label: "Priority", isSortable: true },
    {
      key: "actions",
      label: "Actions",
      isSortable: false,
      content: (category) => (
        <Actions
          onClick={() => this.props.onDelete(category)}
          path="category"
          item={category}
        />
      ),
    },
  ];

  render() {
    const { categories, startNumber, onSort, sortColumn } = this.props;
    return (
      <Table
        columns={this.columns}
        data={categories}
        sortColumn={sortColumn}
        onSort={onSort}
        startNumber={startNumber}
      />
    );
  }
}

// Typechecking
CategoriesTable.propTypes = {
  categories: PropTypes.array.isRequired,
  startNumber: PropTypes.number.isRequired,
  onSort: PropTypes.func.isRequired,
  sortColumn: PropTypes.object.isRequired,
};

export default CategoriesTable;
