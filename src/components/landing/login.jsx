// Libraries
import React from "react";
import { Redirect } from "react-router-dom";
import Joi from "joi-browser";

// Child Components
import Form from "../common/form";
import auth from "../../services/authService";

class Login extends Form {
  state = {
    data: { email: "", password: "" },
    errors: { email: "", password: "" },
    remember: false,
  };

  schema = {
    email: Joi.string().email().required().label("Email"),
    password: Joi.string().required().label("Password"),
  };

  doSubmit = async () => {
    try {
      const { data, remember } = this.state;
      await auth.login(data.email, data.password, remember);
      const { state } = this.props.location;

      window.location = state ? state.from.pathname : "/dashboard/";
    } catch (ex) {
      if (ex.response && ex.response.status === 400) {
        const errors = { ...this.state.errors };
        errors.email = ex.response.data;

        this.setState({ errors });
      }
    }
  };

  render() {
    if (auth.getCurrentUser()) {
      return <Redirect to="/dashboard/" />;
    }

    return (
      <div className="col alt-table-responsive login">
        <div className="row align-items-center d-flex justify-content-center">
          <div className="col-sm-9 col-md-5 col-lg-5 col-xl-4">
            <div className="card card-signin my-5">
              <div className="card-body">
                <h3 className="card-title text-center">Histocaching</h3>

                <hr className="mb-4 mt-2 pb-3" />

                <form className="form-signin pb-3" onSubmit={this.handleSubmit}>
                  {this.renderInput("email", "Email")}
                  {this.renderPassword("password", "Password")}
                  {this.renderCheckbox("Remember me")}
                  <div className="mt-4 pt-3 text-center">
                    {this.renderButton(
                      "login",
                      "btn btn-lg btn-warning btn-block text-uppercase",
                      ""
                    )}
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Login;
