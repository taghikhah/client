import _ from "lodash";

export function paginate(items, pageNumber, pageSize) {
  const startIndex = (pageNumber - 1) * pageSize;
  //const endIndex = Math.min(startIndex + pageSize - 1, pageNumber - 1);
  return _(items).slice(startIndex).take(pageSize).value();
}
