import React from "react";

const Twin = ({ english, german }) => {
  return (
    <div>
      <p>{english}</p>
      <hr />
      <p>{german}</p>
    </div>
  );
};

export default Twin;
