// Libraries
import React from "react";
import PropTypes from "prop-types";

const TextBox = ({ name, label, error, value, ...rest }) => {
  return (
    <div className="form-group" style={{ minHeight: "100%" }}>
      <label htmlFor={name}>{label}</label>
      <textarea
        {...rest}
        id={name}
        key={label}
        name={name}
        value={value}
        className="form-control rounded-box text-box"
      />
      {error && <div className="alert alert-danger">{error}</div>}
    </div>
  );
};

// Typechecking
TextBox.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
};

export default TextBox;
