// Libraries
import React from "react";
import Joi from "joi-browser";
import { toast } from "react-toastify";
// import _ from "lodash";

// Parent Component
import Form from "../common/form";
import Back from "../common/form/back";

// Functions
import { getPois } from "../../services/poiService";
import { uploadFiles } from "../../services/fileService";
import {
  getItem,
  saveItem,
  getItemsMaxPriority,
} from "../../services/itemService";

class ItemForm extends Form {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleFileSelect = this.handleFileSelect.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleImageLoad = this.handleImageLoad.bind(this);
    this.handleLabel = this.handleLabel.bind(this);
    this.handleMapClick = this.handleMapClick.bind(this);

    this.state = {
      data: {
        poi_id: "",
        priority: 1,
        description_en: "",
        description_de: "",
        caption_en: "",
        caption_de: "",
        filename: "",
        image_aspect_ratio: "",
      },
      pois: [],
      selectedFiles: { filename: "" },
      selectedImages: [],
      previewImages: [],
      images: [],
      inputLabels: { filename: "" },
      labels: [],
      dimensions: {},
      errors: {},
      maxPriority: "",
    };
  }

  // Define the Form Schema
  schema = {
    _id: Joi.string(),
    poi_id: Joi.string().required().label("Histocache"),
    priority: Joi.number().integer().min(1).max(100).label("Priority"),
    description_en: Joi.string()
      .min(10)
      .max(5000)
      .required()
      .label("Description (English)"),
    description_de: Joi.string()
      .min(10)
      .max(5000)
      .required()
      .label("Description (German)"),
    caption_en: Joi.string()
      .min(5)
      .max(500)
      .required()
      .label("Caption (English)"),
    caption_de: Joi.string()
      .min(5)
      .max(500)
      .required()
      .label("Caption (German)"),
    filename: Joi.string().required().label("File Name"),
    image_aspect_ratio: Joi.number()
      .min(0.1)
      .max(4)
      .required()
      .label("Aspect Ratio"),
  };

  // Retrive the Data from the Server
  async componentDidMount() {
    // Get Pois and Modify
    const { data: allPois } = await getPois();
    if (allPois.length === 0) return;

    // When at least a Poi exists in the database:
    const pois = allPois.map(({ title_en: name, ...rest }) => ({
      name,
      ...rest,
    }));

    this.setState({ pois });

    // Get the Given ID
    const item_id = this.props.match.params.id;

    if (item_id === "new") return;

    // Get Item related to the Given ID
    const { data: item } = await getItem(item_id);
    if (!item) return this.props.history.replace("/not-found");

    // Get the maximum Priority
    const { data: max } = await getItemsMaxPriority(item);

    this.setState({
      data: this.mapToViewModel(item),
      maxPriority: max[0].priority,
    });
  }

  // Update the priority after selcting the Poi
  async componentDidUpdate(prevProps, prevState) {
    if (prevProps.match.params.id === "new") {
      if (prevState.data.poi_id !== this.state.data.poi_id) {
        const data = { ...this.state.data };
        const { data: max } = await getItemsMaxPriority(data);

        if (max.length === 0) {
          data.priority = 1;
          return this.setState({ data, maxPriority: 1 });
        } else {
          data.priority = max[0].priority + 1;
          return this.setState({ data, maxPriority: max[0].priority });
        }
      }
    }
  }

  // Map current Item to the Form Inputs
  mapToViewModel(item) {
    return {
      _id: item._id,
      priority: item.priority,
      poi_id: item.poi_id,
      description_en: item.description_en,
      description_de: item.description_de,
      caption_en: item.caption_en,
      caption_de: item.caption_de,
      filename: item.filename,
      image_aspect_ratio: item.image_aspect_ratio,
    };
  }

  // Form submit Function
  doSubmit = async () => {
    // Save Item to the Database
    try {
      // Item to save
      const response = await saveItem(this.state.data);
      if (response.data && response.status === 200) {
        toast.success(
          <div>
            Item of priority: "{response.data.priority}"
            <br /> saved successfully.
          </div>
        );

        // Image to Save
        const selectedImages = [...this.state.selectedImages];
        // Image Upload Header
        const header = this.state.data.poi_id;
        const index = selectedImages.length - 1;

        await uploadFiles(selectedImages[index], header, () => {});
      }
    } catch (ex) {
      if (ex.response && ex.response.status === 400) {
        toast.error("This Item has already exists!");
      }
      return;
    }

    // Return to Items TableView after saving
    this.props.history.push("/dashboard/items");
  };

  // Back button Event Handler
  handleBack = () => {
    this.props.history.push("/dashboard/items");
  };

  render() {
    const id = this.props.match.params.id;

    return (
      <div className="col alt-table-responsive mt-1 mb-1">
        <div className="row align-items-center mt-1 mb-2 underline">
          <div className="col mt-2">
            <h2 className="mt-2">
              <i className="bx bx-window"></i>{" "}
              {id === "new" ? "New Item" : "Edit Item Details"}
            </h2>
          </div>
        </div>

        <form
          onSubmit={this.handleSubmit}
          className="form-group"
          encType="multipart/form-data"
        >
          <div className="row align-items-center pt-2 my-2">
            <div className="col-sm-6">
              {this.renderSelect("poi_id", "Histocache", this.state.pois)}
              {this.renderPriority("priority", "Priority")}
              {this.renderFileInput("filename")}
              <div className="row mt-3">
                <div className="col-sm-4">
                  {this.renderInput(
                    "image_aspect_ratio",
                    "Aspect Ratio",
                    "number",
                    "",
                    true
                  )}
                </div>
              </div>
            </div>
            <div className="col-sm-6">
              {this.renderSinglePreview("image_aspect_ratio", "filename")}
            </div>
          </div>

          <hr />

          <div className="row">
            <div className="col-sm-6">
              {this.renderTextBox("description_en", "Description (English)")}
            </div>

            <div className="col-sm-6">
              {this.renderTextBox("description_de", "Description (German)")}
            </div>
          </div>
          <div className="row">
            <div className="col-sm-6">
              {this.renderTextBox("caption_en", "Caption (English)")}
            </div>

            <div className="col-sm-6">
              {this.renderTextBox("caption_de", "Caption (German)")}
            </div>
          </div>

          <hr />

          <div className="row justify-content-between align-items-center mt-3 pb-2">
            <div className="col">
              <Back onClick={this.handleBack} />
            </div>

            <div className="col text-end">
              {this.renderButton("Save", "btn btn-success rounded")}
            </div>
          </div>
        </form>
      </div>
    );
  }
}

export default ItemForm;
