// Libraries
import React from "react";
import PropTypes from "prop-types";

// Child Components
import TableBody from "./table/tableBody";
import TableHeader from "./table/tableHeader";

const Table = ({ columns, sortColumn, onSort, data, startNumber }) => {
  return (
    <table className="table">
      <TableHeader columns={columns} sortColumn={sortColumn} onSort={onSort} />
      <TableBody data={data} columns={columns} startNumber={startNumber} />
    </table>
  );
};

// Typechecking
Table.propTypes = {
  columns: PropTypes.array.isRequired,
  onSort: PropTypes.func.isRequired,
  data: PropTypes.array.isRequired,
  startNumber: PropTypes.number.isRequired,
};

export default Table;
