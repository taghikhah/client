// Libraries
import React, { Component } from "react";
import PropTypes from "prop-types";

// Child Components
import Table from "../common/table";
import Actions from "../common/table/actions";
import Twin from "../common/table/twin";
import Thumbnail from "../common/table/thumbnail";
import Map from "../common/map";

class PoisTable extends Component {
  columns = [
    {
      path: "filename",
      label: "Image",
      isSortable: false,
      content: (poi) => (
        <Thumbnail id={poi._id} filename={poi.filename} alt={poi.title_en} />
      ),
    },
    {
      path: "lat",
      label: "Location",
      isSortable: false,
      content: (poi) => (
        <Map
          lat={poi.lat}
          long={poi.long}
          zoom={16}
          center={[poi.lat, poi.long]}
          style={{ height: "200px", width: "200px" }}
        />
      ),
    },
    {
      path: "title_en",
      label: "Title",
      isSortable: true,
      content: (poi) => <Twin german={poi.title_de} english={poi.title_en} />,
    },
    {
      path: "category.name",
      label: "Category",
      isSortable: true,
      content: (poi) => (
        <Twin german={poi.category.name_de} english={poi.category.name} />
      ),
    },

    { path: "priority", label: "Priority", isSortable: true },
    {
      key: "actions",
      label: "Actions",
      isSortable: false,
      content: (poi) => (
        <Actions
          onClick={() => this.props.onDelete(poi)}
          path="poi"
          item={poi}
        />
      ),
    },
  ];

  render() {
    const { pois, startNumber, onSort, sortColumn } = this.props;
    return (
      <Table
        columns={this.columns}
        data={pois}
        sortColumn={sortColumn}
        onSort={onSort}
        startNumber={startNumber}
      />
    );
  }
}

// Typechecking
PoisTable.propTypes = {
  pois: PropTypes.array.isRequired,
  startNumber: PropTypes.number.isRequired,
  onSort: PropTypes.func.isRequired,
  sortColumn: PropTypes.object.isRequired,
};

export default PoisTable;
