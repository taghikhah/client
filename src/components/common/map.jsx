// Libraries
import React, { Component } from "react";
import PropTypes from "prop-types";
import GoogleMapReact from "google-map-react";

// Config
import { mapStyle } from "../../utils/scripts/map";

// Constants
const googleMapKey = process.env.REACT_APP_GOOGLE_MAP_KEY;

class Map extends Component {
  static defaultProps = {
    center: {
      lat: 51.0504,
      lng: 13.7273,
    },
    zoom: 14,
  };

  render() {
    const {
      onClick,
      lat,
      long,
      zoom,
      style,
      center = [lat, long],
    } = this.props;

    function createMapOptions(maps) {
      return {
        zoomControlOptions: {
          position: maps.ControlPosition.LEFT_TOP,
          style: maps.ZoomControlStyle.SMALL,
        },
        mapTypeControlOptions: {
          position: maps.ControlPosition.TOP_RIGHT,
        },
        mapTypeControl: false,
        streetViewControl: true,
        styles: mapStyle,
        rotateControl: false,
        fullscreenControl: false,
        scrollwheel: true,
        disableDoubleClickZoom: true,
        draggableCursor: "crosshair",
      };
    }

    return (
      // Important! Always set the container height explicitly
      <div style={style}>
        <GoogleMapReact
          onClick={onClick}
          bootstrapURLKeys={{
            key: googleMapKey,
          }}
          defaultCenter={center}
          defaultZoom={zoom}
          options={createMapOptions}
        >
          <i
            className="fa fa-map-pin fa-3x"
            aria-hidden="true"
            style={{ color: "#fec000" }}
            lat={lat}
            lng={long}
          ></i>
        </GoogleMapReact>
      </div>
    );
  }
}

// Typechecking
Map.propTypes = {
  lat: PropTypes.number.isRequired,
  long: PropTypes.number.isRequired,
  zoom: PropTypes.number.isRequired,
  onClick: PropTypes.func,
};

export default Map;
