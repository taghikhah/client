// Libraries
import React, { Component } from "react";
import { NavLink } from "react-router-dom";

class Footer extends Component {
  render() {
    const url = window.location.search;
    if (url === "") {
      return (
        <nav className="navbar navbar-expand-lg  navbar-light bg-warning justify-content-between">
          <span className="navbar-text">
            Copyright © Histocaching by{" "}
            <a
              href="https://tu-dresden.de/ing/informatik/smt/cgv"
              target="_blank"
            >
              CGV
            </a>{" "}
            TU Dresden |{" "}
            <a href="https://www.stasi-unterlagen-archiv.de/" target="_blank">
              StUA
            </a>{" "}
            2021. All rights reserved.
          </span>

          <div
            className="collapse navbar-collapse navbar-light bg-warning"
            id="navbarText"
          >
            <ul className="navbar-nav ml-auto">
              <li className="nav-item">
                <NavLink className="nav-link" to="/imprint">
                  Imprint
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink className="nav-link" to="/privacy-policy">
                  Privacy Policy
                </NavLink>
              </li>
            </ul>
          </div>
        </nav>
      );
    } else {
      return (
        <nav className="navbar navbar-expand-lg  navbar-light bg-warning justify-content-between">
          <span className="navbar-text">
            Copyright © Histocaching beim{" "}
            <a
              href="https://tu-dresden.de/ing/informatik/smt/cgv"
              target="_blank"
            >
              CGV
            </a>{" "}
            TU Dresden |{" "}
            <a href="https://www.stasi-unterlagen-archiv.de/" target="_blank">
              StUA
            </a>{" "}
            2021. Alle Rechte vorbehalten.
          </span>

          <div
            className="collapse navbar-collapse navbar-light bg-warning"
            id="navbarText"
          >
            <ul className="navbar-nav ml-auto">
              <li className="nav-item">
                <NavLink className="nav-link" to="/imprint?lang=de">
                  Impressum
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink className="nav-link" to="/not-found?lang=de">
                  Datenschutz
                </NavLink>
              </li>
            </ul>
          </div>
        </nav>
      );
    }
  }
}

export default Footer;
