// Libraries
import React, { Component } from "react";
import Joi from "joi-browser";
import _ from "lodash";
import md5 from "md5";

// Child Components
import Select from "./form/select";
import Input from "./form/input";
import Password from "./form/password";
import TextBox from "./form/textBox";
import Toggle from "./form/toggle";
import Map from "./map";

// Constants
const imageUrl = process.env.REACT_APP_IMAGE_URL;

class From extends Component {
  state = { data: {}, errors: {} };

  //---- From Validation ----//
  validate = () => {
    const options = {
      abortEarly: false,
    };
    const { error } = Joi.validate(this.state.data, this.schema, options);
    if (!error) return null;

    const errors = {};
    for (let item of error.details) {
      errors[item.path[0]] = item.message;
    }
    return errors;
  };

  validateProperty = ({ name, value }) => {
    const obj = { [name]: value };
    const schema = { [name]: this.schema[name] };
    const { error } = Joi.validate(obj, schema);
    return error ? error.details[0].message : null;
  };

  //---- Save Button ----//
  handleSubmit = (e) => {
    e.preventDefault();

    const errors = this.validate();
    this.setState({ errors: errors || {} });

    if (errors) return;
    this.doSubmit();
  };

  renderButton = (
    label,
    style = "btn btn-primary",
    icon = "fa fa-floppy-o"
  ) => {
    return (
      <button disabled={this.validate()} className={style}>
        {icon && <i className={icon} aria-hidden="true"></i>} {label}
      </button>
    );
  };

  //---- Text Inputs ----//
  handleChange = ({ currentTarget: input }) => {
    const errors = { ...this.state.errors };
    const errorMessage = this.validateProperty(input);

    if (errorMessage) {
      errors[input.name] = errorMessage;
    } else {
      delete errors[input.name];
    }

    const data = { ...this.state.data };
    data[input.name] = input.value;
    this.setState({ data, errors });
  };

  renderInput = (name, label, type = "text", text = "", status) => {
    const { data, errors } = this.state;

    return (
      <Input
        name={name}
        key={name}
        label={label}
        type={type}
        text={text}
        status={status}
        value={data[name] || ""}
        onChange={this.handleChange}
        error={errors[name]}
      />
    );
  };

  renderNumberInput = (name, label, type = "text", text = "", status) => {
    const { data, errors } = this.state;

    return (
      <Input
        name={name}
        key={name}
        label={label}
        type={type}
        text={text}
        status={status}
        value={data[name] || 0}
        step="0.1"
        onChange={this.handleChange}
        error={errors[name]}
      />
    );
  };

  renderPassword = (name, label, text = "", status) => {
    const { data, errors } = this.state;

    return (
      <Password
        name={name}
        key={name}
        label={label}
        text={text}
        status={status}
        value={data[name] || ""}
        onChange={this.handleChange}
        error={errors[name]}
      />
    );
  };

  renderTextBox = (name, label, type = "text") => {
    const { data, errors } = this.state;

    return (
      <TextBox
        name={name}
        label={label}
        key={name}
        type={type}
        value={data[name]}
        onChange={this.handleChange}
        error={errors[name]}
      />
    );
  };

  renderSelect = (name, label, options, status = false) => {
    const { data, errors } = this.state;

    return (
      <Select
        name={name}
        label={label}
        options={options}
        status={status}
        value={data[name]}
        onChange={this.handleChange}
        error={errors[name]}
      />
    );
  };

  renderPriority = (name, label) => {
    const { data, errors, maxPriority } = this.state;

    return (
      <Input
        name={name}
        key={name}
        label={label}
        type={"number"}
        max={maxPriority + 1}
        min={1}
        text={""}
        status={false}
        value={data[name] || ""}
        onChange={this.handleChange}
        error={errors[name]}
      />
    );
  };

  //---- Switch Button ----//
  handleSwitchChange = (name) => {
    const data = { ...this.state.data };

    data[name] = !this.state.data[name];

    this.setState({ data });
  };

  renderToggle = (name, label) => {
    const { data } = this.state;

    return (
      <Toggle
        name={name}
        label={label}
        checked={data[name]}
        onChange={() => this.handleSwitchChange(name)}
      />
    );
  };

  //---- Checkbox Button ----//
  handleCheckbox = () => {
    let remember = { ...this.state.remember };

    remember = !this.state.remember;

    this.setState({ remember });
  };

  renderCheckbox = (label) => {
    return (
      <div className="form-check custom-control custom-checkbox mt-2 mb-3">
        <input
          className="form-check-input"
          type="checkbox"
          value=""
          id="flexCheckChecked"
          onChange={this.handleCheckbox}
        />
        <label className="form-check-label" htmlFor="flexCheckChecked">
          {label}
        </label>
      </div>
    );
  };

  //---- Map Input ----//
  handleMapClick = (e, lat, long) => {
    const data = { ...this.state.data };

    data[lat] = e.lat;
    data[long] = e.lng;

    this.setState({ data });
  };

  renderMap = (lat, long) => {
    const { data } = this.state;
    let latitude = data[lat] > 51 ? data[lat] : 51.0502;
    let longitude = data[long] > 13 ? data[long] : 13.7385;
    return (
      <Map
        onClick={(e) => this.handleMapClick(e, lat, long)}
        lat={data[lat] > 51 ? data[lat] : 51.0502}
        long={data[long] > 13 ? data[long] : 13.7385}
        zoom={15}
        center={[latitude, longitude]}
        style={{ height: "351px", width: "100%" }}
      />
    );
  };

  //---- Image Input ----//
  handleFileSelect(event) {
    event.preventDefault();

    const selectedFiles = { ...this.state.selectedFiles };
    selectedFiles[event.target.id] = URL.createObjectURL(event.target.files[0]);

    const inputLabels = { ...this.state.inputLabels };
    inputLabels[event.target.id] = event.target.files[0].name;

    if (inputLabels["filename"] && inputLabels["viewpoint_filename"]) {
      const selectedImages = [...this.state.selectedImages];

      if (event.target.id === "filename") {
        selectedImages[0] = event.target.files[0];
      } else {
        selectedImages[1] = event.target.files[0];
      }

      this.setState({ selectedImages });
    } else {
      const selectedImages = [...this.state.selectedImages];

      if (selectedImages.length !== 0) {
        selectedImages.shift();
      }
      selectedImages.push(event.target.files[0]);

      this.setState({ selectedImages });
    }

    this.setState({ selectedFiles, inputLabels });

    // Images
    const images = [];

    for (let i = 0; i < event.target.files.length; i++) {
      images.push(URL.createObjectURL(event.target.files[i]));
    }

    this.setState({ previewImages: images });

    if (event.target.files) {
      const fileNameObject = _.mapValues(event.target.files, function (o) {
        return o.name;
      });

      const labels = Object.keys(fileNameObject).map(function (key) {
        return fileNameObject[key];
      });

      this.setState({ labels });
    }
  }

  handleImageLoad = ({ target: img }) => {
    const dimensions = { ...this.state.dimensions };

    const data = { ...this.state.data };

    if (img.id !== "new") {
      dimensions.height = img.offsetHeight;
      dimensions.width = img.offsetWidth;

      const aspectRatio = dimensions.width / dimensions.height;

      data[img.id] = Math.round(aspectRatio * 100) / 100;
    }

    // Asign filenames to the data
    const inputLabels = { ...this.state.inputLabels };
    if (inputLabels["filename"]) {
      data.filename = this.fileNameHash(inputLabels["filename"]);
    }
    if (inputLabels["viewpoint_filename"]) {
      data.viewpoint_filename = this.fileNameHash(
        inputLabels["viewpoint_filename"]
      );
    }

    this.setState({ dimensions, data });
  };

  handlePreviewImage = (selectedFiles, data, aspectRatio, name) => {
    if (selectedFiles[name]) {
      return (
        <img
          className="image-preview"
          src={selectedFiles[name]}
          id={aspectRatio}
          alt={name}
          key={name}
          onLoad={this.handleImageLoad}
        />
      );
    } else if (data[name]) {
      return (
        <img
          className="image-preview"
          src={`${imageUrl}/${data._id}/${data[name]}`}
          id={aspectRatio}
          alt={name}
          key={name}
          onLoad={this.handleImageLoad}
        />
      );
    }
  };

  handleLabel = (labels, data) => {
    if (labels) {
      return labels;
    } else if (data) {
      return data;
    } else {
      return "Choose a file from device ...";
    }
  };

  fileNameHash = (name) => {
    const index = name.lastIndexOf(".");
    const uniqueName = md5(name);
    const filename = uniqueName + name.slice(index);
    return filename.toLowerCase();
  };

  renderFileInput = (name) => {
    const { data, inputLabels } = this.state;

    return (
      <React.Fragment>
        <label htmlFor={name}>Filename</label>
        <div className="custom-file">
          <input
            type="file"
            className="custom-file-input"
            id={name}
            key={name}
            onChange={this.handleFileSelect}
          />
          <label className="custom-file-label rounded" htmlFor="customFile">
            {this.handleLabel(inputLabels[name], data[name])}
          </label>
        </div>
      </React.Fragment>
    );
  };

  renderPreview = (aspectRatio, name) => {
    const { selectedFiles, data } = this.state;

    return (
      <div className="card placeholder" style={{ height: "265px" }}>
        <div className="card-body placeholder-body">
          <div className="vertical-scroll" style={{ maxHeight: "265px" }}>
            {this.handlePreviewImage(selectedFiles, data, aspectRatio, name)}
          </div>
        </div>
      </div>
    );
  };

  renderSinglePreview = (aspectRatio, name) => {
    const { selectedFiles, data } = this.state;

    let source = "";

    if (selectedFiles[name]) {
      source = selectedFiles[name];
    } else if (data.poi_id && data[name]) {
      source = `${imageUrl}/${data.poi_id}/${data[name]}`;
    }

    return (
      <div className="card placeholder" style={{ height: "265px" }}>
        <div className="card-body placeholder-body">
          <div className="vertical-scroll" style={{ maxHeight: "265px" }}>
            <img
              className="image-preview"
              src={source}
              id={aspectRatio}
              alt={data[name] || selectedFiles[name]}
              key={data[name] || selectedFiles[name]}
              onLoad={this.handleImageLoad}
            />
          </div>
        </div>
      </div>
    );
  };

  renderImage = (filename) => {
    return (
      <div className="placeholder">
        <div className="alternative"></div>
        <img className="histocache-image" src="<imgUrl>" alt={filename} />
      </div>
    );
  };

  renderProgress = () => {
    const { progresses } = this.state;
    return (
      progresses &&
      progresses.map((p, index) => (
        <div className=" mb-2" key={index}>
          <div style={{ width: "100%" }}>
            <label style={{ display: "block" }}>{p.fileName}</label>
          </div>
          <div>
            <div className="progress">
              <div
                className="progress-bar progress-bar-info roundify"
                role="progressbar"
                aria-valuenow={p.percentage}
                aria-valuemin="0"
                aria-valuemax="100"
                style={{ width: p.percentage + "%" }}
              >
                {p.percentage}%
              </div>
            </div>
          </div>
        </div>
      ))
    );
  };
}

export default From;
