// Libraries
import React, { Component } from "react";
import { Link } from "react-router-dom";

// Child Components
import Role from "../common/misc/role";
import Format from "../common/misc/format";
import Back from "../common/form/back";

// Functions
import userService from "../../services/userService";

class Profile extends Component {
  state = { account: "" };

  async componentDidMount() {
    const { data: account } = await userService.findUser(this.props.user._id);
    this.setState({ account });
  }

  handleBack = () => {
    this.props.history.push("/dashboard/users");
  };

  render() {
    const { account } = this.state;

    return (
      <div className="col alt-table-responsive mt-1 mb-1">
        <div className="row align-items-center mt-1 mb-2 underline">
          <div className="col mt-2">
            <h2 className="mt-2">
              <i className="bx bx-user"></i> Profile
            </h2>
          </div>
        </div>

        <div className="row align-items-center mx-2 py-1 my-3">
          {account && (
            <div className="col-sm-6">
              <h5>
                <Role role={account.role} />
              </h5>
              <div className="py-3">
                <h6>Name</h6>
                <h3>{account.name}</h3>
              </div>
              <div className="py-3">
                <h6>Email Address</h6>
                <h5>
                  <a href={`mailto:${account.email}`}>{account.email}</a>
                </h5>
              </div>
              <div className="py-3">
                <h6>Created</h6>

                <h5>
                  <Format date={account.created_at} />
                </h5>
              </div>
            </div>
          )}
          <div className="col-sm-6">
            <div className="container">
              <div className="content">
                <div className="card rounded-semi" style={{ width: "100%" }}>
                  <div className="card-body">
                    <img
                      src={`${process.env.PUBLIC_URL}/profile.png`}
                      alt="profile"
                      style={{ width: "100%" }}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <hr />
        <div className="row justify-content-between align-items-center mt-3 pb-2">
          <div className="col">
            <Back onClick={this.handleBack} />
          </div>

          <div className="col text-end">
            <Link
              className="btn btn-warning rounded"
              to={`/dashboard/user/${account._id}`}
            >
              <i className="fa fa-pencil" aria-hidden="true"></i> Edit Profile
            </Link>
          </div>
        </div>
      </div>
    );
  }
}

export default Profile;
