// Libraries
import React, { Component } from "react";
import PropTypes from "prop-types";

class Password extends Component {
  state = {
    type: "password",
  };

  handleClick = () =>
    this.setState(({ type }) => ({
      type: type === "text" ? "password" : "text",
    }));

  render() {
    const { name, label, error, value, text, status, ...rest } = this.props;
    return (
      <div className="form-group">
        <label htmlFor={name}>{label}</label>

        <div className="input-group mb-3">
          <input
            {...rest}
            id={name}
            name={name}
            value={value}
            className="form-control left-rounded"
            type={this.state.type}
            disabled={false ^ status}
          />
          <div className="input-group-append">
            <button
              className="btn btn-outline-secondary right-rounded outline"
              type="button"
              onClick={this.handleClick}
            >
              {this.state.type === "text" ? (
                <i className="fa fa-eye-slash v-top" aria-hidden="true"></i>
              ) : (
                <i className="fa fa-eye v-top" aria-hidden="true"></i>
              )}
            </button>
          </div>
          <small className="w-100" style={{ color: "red" }}>
            {text}
          </small>
        </div>

        {error && <div className="alert alert-danger">{error}</div>}
      </div>
    );
  }
}

// Typechecking
Password.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  status: PropTypes.bool,
};

export default Password;
