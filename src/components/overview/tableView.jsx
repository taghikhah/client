// Libraries
import React, { Component } from "react";
import PropTypes from "prop-types";

// Child Components
import Table from "../common/table";
import Twin from "../common/table/twin";
import Type from "../common/misc/type";
import LighBox from "../common/misc/lightBox";
import Thumbnail from "../common/table/thumbnail";

class TableView extends Component {
  columns = [
    {
      path: "name",
      label: "Image",
      isSortable: false,
      content: (image) => <Thumbnail filename={image.url} alt={image.name} />,
    },

    {
      path: "poi.title_en",
      label: "Histocache",
      isSortable: true,
      content: (image) => (
        <Twin german={image.poi.title_de} english={image.poi.title_en} />
      ),
    },
    {
      path: "poi.category.name_en",
      label: "Category",
      isSortable: true,
      content: (image) => (
        <Twin
          german={image.poi.category.name_de}
          english={image.poi.category.name_en}
        />
      ),
    },
    {
      path: "type",
      label: "Type",
      isSortable: true,
      content: (image) => <Type type={image.type} />,
    },
    {
      path: "actions",
      label: "Actions",
      isSortable: false,
      content: (image) => <LighBox image={image} />,
    },
  ];

  render() {
    const { images, startNumber, onSort, sortColumn } = this.props;
    return (
      <Table
        columns={this.columns}
        data={images}
        sortColumn={sortColumn}
        onSort={onSort}
        startNumber={startNumber}
      />
    );
  }
}

// Typechecking
TableView.propTypes = {
  images: PropTypes.array.isRequired,
  startNumber: PropTypes.number.isRequired,
  onSort: PropTypes.func.isRequired,
  sortColumn: PropTypes.object.isRequired,
};

export default TableView;
