// Libraries
import React, { Component } from "react";
import { Link } from "react-router-dom";
import { toast } from "react-toastify";
import _ from "lodash";

// Common Components
import Search from "../common/search";
import Pagination from "../common/pagination";
import TableMeta from "../common/table/tableMeta";

// Child Components
import CategoriesTable from "./categoriesTable";
import Expire from "../common/expire";

// Functions
import { getCategories, deleteCategory } from "../../services/categoryService";
import { paginate } from "../../utils/scripts/paginate";
import { getPois } from "../../services/poiService";

class Categories extends Component {
  state = {
    categories: [],
    currentPage: 1,
    initialSize: 5,
    pageSize: 5,
    searchQuery: "",
    sortColumn: { path: "name_en", order: "asc" },
  };

  // Retrive the Data from the Server
  async componentDidMount() {
    // Get Categories
    const { data: categories } = await getCategories();

    this.setState({ categories });
  }

  // Delete Event Handler
  handleDelete = async (category) => {
    // Delete Validation
    const { data: pois } = await getPois();
    const result = pois.filter((p) => p.category_id === category._id);
    if (result.length !== 0) {
      toast.error(
        <div>
          Not allowed!
          <br /> First, delete the {result.length} Histocache(s) of this
          Category.
        </div>
      );
      return;
    }

    // Delete the Category from the State
    const originalCategories = this.state.categories;
    const categories = originalCategories.filter((c) => c._id !== category._id);

    this.setState({ categories });

    try {
      const response = await deleteCategory(category._id);
      if (response.data && response.status === 200) {
        toast.success(
          <div>
            Category: "{response.data.name_en}"
            <br /> deleted successfully.
          </div>
        );
      }
    } catch (ex) {
      if (ex.response && ex.response.status === 401) {
        toast.error("Only Admins can delete a Category!");
      } else if (ex.response && ex.response.status === 404) {
        toast.error("This Category has already been deleted!");
      }

      this.setState({ categories: originalCategories });
    }
  };

  // Pagination event handler
  handlePageChange = (page) => {
    this.setState({ currentPage: page });
  };

  // Page size event handler
  handleSizeChange = ({ currentTarget: input }) => {
    let pageSize = { ...this.state.pageSize };
    pageSize = input.value;
    this.setState({ pageSize });
  };

  // Search event handler
  handleSearch = (query) => {
    this.setState({
      searchQuery: query,
      selectedCategory: null,
      currentPage: 1,
    });
  };

  // Sort event handler
  handleSort = (sortColumn) => {
    this.setState({ sortColumn });
  };

  getData = () => {
    const {
      currentPage,
      pageSize,
      categories: allCategories,
      sortColumn,
      searchQuery,
    } = this.state;

    // Start number
    const startNumber = (currentPage - 1) * pageSize;

    // Filter option
    let filtered = allCategories;
    if (searchQuery)
      filtered = allCategories.filter(
        (c) =>
          c.name_en.toLowerCase().indexOf(searchQuery.toLowerCase()) !== -1 ||
          c.name_de.toLowerCase().indexOf(searchQuery.toLowerCase()) !== -1
      );

    // Sorting option
    const sorted = _.orderBy(filtered, [sortColumn.path], [sortColumn.order]);

    // Pagination option
    const categories = paginate(sorted, currentPage, pageSize);

    return {
      startNumber: startNumber,
      totalCount: filtered.length,
      data: categories,
    };
  };

  render() {
    const { length: count } = this.state.categories;
    const { currentPage, pageSize, sortColumn, searchQuery, initialSize } =
      this.state;
    const { startNumber, totalCount, data: categories } = this.getData();

    return (
      <div className="col alt-table-responsive mt-1">
        <div className="row justify-content-between align-items-center underline my-2">
          <div className="col-md-6 mt-2">
            <h2>
              <i className="bx bx-category-alt"></i> Categories
            </h2>
          </div>
          <div className="col-sm-12 col-md-5 col-lg-4 col-xl-3 mb-2 text-end ">
            <Link
              className="btn btn-warning btn-block rounded"
              to="/dashboard/category/new"
            >
              <i className="fa fa-plus-circle" aria-hidden="true"></i> Create a
              new Category
            </Link>
          </div>
        </div>

        {count !== 0 && (
          <React.Fragment>
            <div className="row justify-content-between align-items-center pb-1">
              <div className="col-md-6 my-2"></div>
              <div className="col-sm-12 col-md-5 col-lg-4 col-xl-3 my-2 text-end ">
                <Search value={searchQuery} onChange={this.handleSearch} />
              </div>
            </div>

            <div className="row outer mt-1">
              <div className="col">
                <CategoriesTable
                  categories={categories}
                  sortColumn={sortColumn}
                  startNumber={startNumber}
                  onDelete={this.handleDelete}
                  onSort={this.handleSort}
                />
              </div>
            </div>
          </React.Fragment>
        )}

        {count === 0 && (
          <div className="row">
            <div className="col-sm-12 align-self-center">
              <Expire delay={5000}>
                <div className="loader">
                  <div></div>
                  <div></div>
                  <div></div>
                </div>
              </Expire>
            </div>
          </div>
        )}

        <hr />

        <div className="row align-items-center">
          <div className="col-sm-12 col-md col-lg col-xl mr-auto pb-1">
            <Pagination
              itemsCount={totalCount}
              pageSize={pageSize}
              currentPage={currentPage}
              onPageChange={this.handlePageChange}
            />
          </div>
          <div className="col-auto align-middle">
            <TableMeta
              initial={initialSize}
              itemsCount={totalCount}
              total={count}
              onChange={this.handleSizeChange}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default Categories;
