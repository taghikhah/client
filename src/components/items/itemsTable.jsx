// Libraries
import React, { Component } from "react";
import PropTypes from "prop-types";

// Child Components
import Table from "../common/table";
import Twin from "../common/table/twin";
import Thumbnail from "../common/table/thumbnail";
import Actions from "../common/table/actions";

class ItemsTable extends Component {
  columns = [
    {
      path: "filename",
      label: "Item Image",
      isSortable: false,
      content: (item) => (
        <Thumbnail
          id={item.poi._id}
          filename={item.filename}
          alt={item.title_en}
        />
      ),
    },
    {
      path: "poi.title_en",
      label: "Histocache",
      isSortable: true,
      content: (item) => (
        <Twin german={item.poi.title_de} english={item.poi.title_en} />
      ),
    },
    { path: "priority", label: "Priority", isSortable: true },
    {
      key: "actions",
      label: "Actions",
      isSortable: false,
      content: (item) => (
        <Actions
          onClick={() => this.props.onDelete(item)}
          path="item"
          item={item}
        />
      ),
    },
  ];

  render() {
    const { items, startNumber, onSort, sortColumn } = this.props;
    return (
      <Table
        columns={this.columns}
        data={items}
        sortColumn={sortColumn}
        onSort={onSort}
        startNumber={startNumber}
      />
    );
  }
}

// Typechecking
ItemsTable.propTypes = {
  items: PropTypes.array.isRequired,
  startNumber: PropTypes.number.isRequired,
  onSort: PropTypes.func.isRequired,
  sortColumn: PropTypes.object.isRequired,
};

export default ItemsTable;
