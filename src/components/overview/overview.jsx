// Libraries
import React, { Component } from "react";
import _ from "lodash";

// Common Components
import TableMeta from "../common/table/tableMeta";
import Pagination from "../common/pagination";

// Child Components
import MapView from "./mapView";
import TableView from "./tableView";
import Expire from "../common/expire";

// Functions
import { paginate } from "../../utils/scripts/paginate";
import { getFiles } from "../../services/fileService";
import { getPois } from "../../services/poiService";
import { getCategories } from "../../services/categoryService";

class OverView extends Component {
  state = {
    images: [],
    pois: [],
    currentPage: 1,
    initialSize: 3,
    pageSize: 3,
    selectedPoi: null,
    sortColumn: { path: "poi.title_en", order: "asc" },
  };

  // Retrive data from the server
  async componentDidMount() {
    // Get Categories and Modify
    const { data: allCategories } = await getCategories();
    if (allCategories.length === 0) return;

    // Get Pois and Modify
    const { data: allPois } = await getPois();
    if (allPois.length === 0) return;

    const data = allPois.map(({ title_en: name, ...rest }) => ({
      name,
      ...rest,
    }));

    // Remove pois that doesn't have location
    const poisWithlocation = data.filter(
      (p) => p.has_histocache_location === true
    );

    const pois = [...poisWithlocation];

    // Mapping Functions
    function getGategory(id) {
      var category = allCategories.find((c) => {
        return c._id === id;
      });
      if (category) return category;
    }

    function getPoi(id) {
      var poi = allPois.find((p) => {
        return p._id === id;
      });
      if (poi) return poi;
    }

    function getType(name) {
      let type = "";
      let result;

      allPois.forEach((p) => {
        if (p.filename === name) {
          result = "main";
        } else if (p.viewpoint_filename === name) {
          result = "viewpoint";
        }
      });

      switch (result) {
        case "main":
          type = "Histocache";
          break;
        case "viewpoint":
          type = "Viewpoint";
          break;

        default:
          type = "Item Image";
      }

      return type;
    }

    // Get Images and Modify
    const { data: allImages } = await getFiles();
    if (allImages.length === 0) return;

    // Remove -thumb images from the array
    const filteredImages = allImages.filter(
      (i) => i.name.search("thumb") === -1
    );

    // Make a new array of image objects
    const imagesWithPois = filteredImages.map(({ parent: poi, name, url }) => ({
      poi: {
        _id: getPoi(poi)._id,
        title_en: getPoi(poi).title_en,
        title_de: getPoi(poi).title_de,
        has_histocache_location: getPoi(poi).has_histocache_location,
        category: {
          name_en: getGategory(getPoi(poi).category_id).name_en,
          name_de: getGategory(getPoi(poi).category_id).name_de,
        },
      },
      type: getType(name),
      name,
      url,
    }));

    // Remove images of pois that doesn't have location
    const images = imagesWithPois.filter(
      (i) => (i.poi.has_histocache_location = true)
    );

    this.setState({ images, pois });
  }

  // Pagination event handler
  handlePageChange = (page) => {
    this.setState({ currentPage: page });
  };

  // Page size event handler
  handleSizeChange = ({ currentTarget: input }) => {
    let pageSize = { ...this.state.pageSize };
    pageSize = input.value;

    this.setState({ pageSize });
  };

  // Marker view classes event handler
  handleMarkerClass = (e) => {
    if (e.target.classList.value === "map-box active") {
      return;
    }

    e.target.classList.toggle("active");
    e.currentTarget.classList.toggle("active");

    document
      .querySelectorAll(".map-tick:not(.active)")
      .forEach((el) => (el.style.display = "none"));
  };

  // Filter Pois on Marker click event handler
  handleMarkerClick = (e) => {
    const selectedPoi = e.target.innerHTML;
    const poi = this.state.pois.find((p) => p.name === selectedPoi);

    const images = this.state.images.filter(
      (i) => i.poi.title_en === selectedPoi
    );

    this.setState({
      selectedPoi: poi,
      searchQuery: "",
      initialSize: images.length,
      pageSize: images.length,
      currentPage: 1,
      sortColumn: { path: "type", order: "asc" },
    });
  };

  // Filter event handler
  handlePoiSelect = ({ currentTarget: input }) => {
    const poi = this.state.pois.find((p) => p._id === input.value);

    this.setState({
      selectedPoi: poi,
      searchQuery: "",
      currentPage: 1,
    });
  };

  // Reset Button Event Handler
  handleReset = () => {
    document
      .querySelectorAll(".active")
      .forEach((el) => el.classList.remove("active"));

    this.setState({
      selectedPoi: null,
      searchQuery: "",
      initialSize: 3,
      pageSize: 3,
      currentPage: 1,
      sortColumn: { path: "poi.title_en", order: "asc" },
    });

    document
      .querySelectorAll(".map-tick:not(.active)")
      .forEach((el) => (el.style.display = "initial"));
  };

  // Search event handler
  handleSearch = (query) => {
    this.setState({
      searchQuery: query,
      selectedPoi: null,
      currentPage: 1,
    });
  };

  // Sort event handler
  handleSort = (sortColumn) => {
    this.setState({ sortColumn });
  };

  getData = () => {
    const {
      currentPage,
      pageSize,
      images: allImages,
      selectedPoi,
      searchQuery,
      sortColumn,
    } = this.state;

    // Start Number
    const startNumber = (currentPage - 1) * pageSize;

    // Filter option
    let filtered = allImages;

    if (searchQuery)
      filtered = allImages.filter((i) =>
        i.name.toLowerCase().startsWith(searchQuery.toLowerCase())
      );
    else if (selectedPoi && selectedPoi._id)
      filtered = allImages.filter((i) => i.poi._id === selectedPoi._id);

    // Sorting option
    const sorted = _.orderBy(filtered, [sortColumn.path], [sortColumn.order]);

    // Pagination option
    const images = paginate(sorted, currentPage, pageSize);

    return {
      startNumber: startNumber,
      totalCount: filtered.length,
      data: images,
    };
  };

  render() {
    const { length: count } = this.state.images;
    const { currentPage, pageSize, sortColumn, initialSize } = this.state;
    const { startNumber, totalCount, data: images } = this.getData();

    return (
      <div className="col alt-table-responsive mt-1">
        <div className="row justify-content-between align-items-center underline my-2">
          <div className="col-md-6 mt-2">
            <h2>
              <i className="bx bx-layer"></i> Overview
            </h2>
          </div>
          <div className="col-sm-12 col-md-5 col-lg-4 col-xl-3 mb-2 text-end">
            <button
              className="btn btn-warning btn-block rounded"
              type="reset"
              onClick={this.handleReset}
            >
              <i className="fa fa-globe" aria-hidden="true"></i> View all
              Histocaches
            </button>
          </div>
        </div>

        {count === 0 && (
          <div className="row">
            <div className="col-sm-12 align-self-center">
              <Expire delay={5000}>
                <div className="loader">
                  <div></div>
                  <div></div>
                  <div></div>
                </div>
              </Expire>
            </div>
          </div>
        )}

        {count !== 0 && (
          <React.Fragment>
            <div className="row py-3">
              <div className="col">
                <MapView
                  pois={this.state.pois}
                  onClick={this.handleMarkerClick}
                  onMouseDown={this.handleMarkerClass}
                />
              </div>
            </div>

            <div className="row outer pt-2">
              <div className="col">
                <TableView
                  images={images}
                  sortColumn={sortColumn}
                  startNumber={startNumber}
                  onSort={this.handleSort}
                  onClick={this.handleMarkerClick}
                />
              </div>
            </div>
          </React.Fragment>
        )}
        <hr />

        <div className="row align-items-center">
          <div className="col-sm-12 col-md col-lg col-xl mr-auto pb-1">
            <Pagination
              itemsCount={totalCount}
              pageSize={pageSize}
              currentPage={currentPage}
              onPageChange={this.handlePageChange}
            />
          </div>
          <div className="col-auto align-middle">
            <TableMeta
              initial={initialSize}
              itemsCount={totalCount}
              total={count}
              onChange={this.handleSizeChange}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default OverView;
