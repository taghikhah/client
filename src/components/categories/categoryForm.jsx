// Libraries
import React from "react";
import Joi from "joi-browser";
import { toast } from "react-toastify";

// Parent Component
import Form from "../common/form";
import Back from "../common/form/back";

// Functions
import {
  getCategory,
  saveCategory,
  getCategoriesMaxPriority,
} from "../../services/categoryService";

class CategoryForm extends Form {
  state = {
    data: {
      priority: "",
      name_de: "",
      name_en: "",
    },
    pois: [],
    errors: {},
    maxPriority: "",
  };

  schema = {
    _id: Joi.string(),
    priority: Joi.number().integer().min(1).max(100).label("Priority"),
    name_de: Joi.string().min(3).max(100).required().label("Name (German)"),
    name_en: Joi.string().min(3).max(100).required().label("Name (English)"),
  };

  // Retrive the Data from the Server
  async componentDidMount() {
    // Get the maximum Priority
    const { data: max } = await getCategoriesMaxPriority();

    // Get the Given ID
    const category_id = this.props.match.params.id;

    if (category_id === "new") {
      const data = { ...this.state.data };

      if (max.length === 0) {
        data.priority = 1;
        return this.setState({ data, maxPriority: 1 });
      } else {
        data.priority = max[0].priority + 1;
        return this.setState({ data, maxPriority: max[0].priority });
      }
    }

    // Get Poi related to the Given ID
    const { data: category } = await getCategory(category_id);
    if (!category) return this.props.history.replace("/not-found");

    this.setState({
      data: this.mapToViewModel(category),
      maxPriority: max[0].priority,
    });
  }

  // Map current Poi to the Form Inputs
  mapToViewModel(category) {
    return {
      _id: category._id,
      priority: category.priority,
      name_en: category.name_en,
      name_de: category.name_de,
    };
  }

  doSubmit = async () => {
    // Save Item to the Database
    try {
      const response = await saveCategory(this.state.data);

      if (response.data && response.status === 200) {
        toast.success(
          <div>
            Category: "{response.data.name_en}"
            <br /> saved successfully.
          </div>
        );
      }
    } catch (ex) {
      if (ex.response && ex.response.status === 400) {
        toast.error("A Category with that name already exists!");
      }
      return;
    }
    this.props.history.push("/dashboard/categories");
  };

  // Back button Event Handler
  handleBack = () => {
    this.props.history.push("/dashboard/categories");
  };

  render() {
    const id = this.props.match.params.id;
    return (
      <div className="col alt-table-responsive mt-1 mb-1">
        <div className="row align-items-center mt-1 mb-2 underline">
          <div className="col mt-2">
            <h2 className="mt-2">
              <i className="bx bx-category"></i>{" "}
              {id === "new" ? "New Category" : "Edit Category Details"}
            </h2>
          </div>
        </div>

        <form onSubmit={this.handleSubmit} className="form-group">
          <div className="row align-items-center pt-2 my-2">
            <div className="col-sm-6">
              {this.renderPriority("priority", "Priority")}
            </div>
            <div className="col-sm-6"></div>
          </div>

          <hr />

          <div className="row">
            <div className="col-sm-6">
              <i className="united states flag"></i>
              {this.renderInput("name_en", "Name (English)")}
            </div>

            <div className="col-sm-6">
              {this.renderInput("name_de", "Name (German)")}
            </div>
          </div>

          <hr />

          <div className="row justify-content-between align-items-center mt-3 pb-2">
            <div className="col">
              <Back onClick={this.handleBack} />
            </div>

            <div className="col text-end">
              {this.renderButton("Save", "btn btn-outline-success rounded")}
            </div>
          </div>
        </form>
      </div>
    );
  }
}

export default CategoryForm;
