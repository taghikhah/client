// Libraries
import React from "react";
import { Switch } from "react-router-dom";

// Child Components
import Public from "./public";
import Private from "./private";
import Dashboard from "../components/dashboard/dashboard";
import Landing from "../components/landing/landing";

// Functions
import authService from "../services/authService";

class Routes extends React.Component {
  state = { user: "" };

  componentDidMount() {
    const user = authService.getCurrentUser();
    this.setState({ user });
  }

  render() {
    return (
      <Switch>
        {this.state.user && (
          <Private
            path="/dashboard"
            ecxact
            render={(props) => <Dashboard {...props} user={this.state.user} />}
          />
        )}
        <Public path="/" component={Landing} />
      </Switch>
    );
  }
}

export default Routes;
