import React from "react";

const renderMeta = (itemsCount, total) => {
  if (itemsCount) {
    if (itemsCount === total) return `Showing all of ${total} entries.`;
    else return `Showing ${itemsCount} of ${total} entries.`;
  }
  return "No entries found!";
};

const TableMeta = ({ initial = 3, itemsCount, total, onChange }) => {
  return (
    <div className="input-group mb-3 ">
      <select
        title="Select Page Size"
        className="custom-select left-rounded col-md-3"
        id={total}
        name={total}
        onChange={onChange}
      >
        <option key={initial} defaultValue={initial}>
          {initial}
        </option>
        <option key={10} value="10">
          10
        </option>
        <option key={20} value="20">
          20
        </option>
        <option key={50} value="50">
          50
        </option>
      </select>
      <div className="input-group-append">
        <label className="input-group-text right-rounded" htmlFor={total}>
          {renderMeta(itemsCount, total)}
        </label>
      </div>
    </div>
  );
};

export default TableMeta;

/*
<div className="transparent-div">
      {renderMeta(startNumber, itemsCount, total)}
    </div>


<div className="input-group mb-3">
      <div className="input-group-prepend dropup">
        <button
          className="btn btn-primary dropdown-toggle left-rounded"
          type="button"
          data-toggle="dropdown"
          aria-haspopup="true"
          aria-expanded="false"
        >
          5
        </button>
        <div className="dropdown-menu">
          <a className="dropdown-item" href="#">
            10
          </a>
          <a className="dropdown-item" href="#">
            20
          </a>
          <a className="dropdown-item" href="#">
            50
          </a>
          <div role="separator" className="dropdown-divider"></div>
          <a className="dropdown-item" href="#">
            100
          </a>
        </div>
      </div>
      <input
        type="text"
        className="form-control right-rounded"
        placeholder={renderMeta(startNumber, itemsCount, total)}
        value={renderMeta(startNumber, itemsCount, total)}
        disabled
      ></input>
    </div>

    */
