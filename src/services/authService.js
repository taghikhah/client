// Libraries
import jwtDecode from "jwt-decode";

// Axios
import http from "./httpService";

// Constants
const apiEndpoint = "/auth";
const tokenKey = process.env.REACT_APP_TOKEN_KEY;

// Handling bi-directional dependencies
http.setJwt(getJwt());

// API login
export async function login(email, password, remember) {
  const { data: jwt } = await http.post(apiEndpoint, { email, password });
  localStorage.setItem(tokenKey, jwt);

  const expiry = Date.now() + 3600000;
  if (!remember) {
    localStorage.setItem("expire", expiry);
  }
}

export function logout() {
  localStorage.removeItem(tokenKey);
  const expiry = localStorage.getItem("expire");
  if (expiry) {
    localStorage.removeItem("expire");
  }
  localStorage.removeItem(tokenKey);
}

export function getCurrentUser() {
  try {
    const expiry = localStorage.getItem("expire");
    const jwt = localStorage.getItem(tokenKey);
    if (expiry) {
      if (Date.now() > expiry) {
        localStorage.removeItem(tokenKey);
        return null;
      }
    }
    return jwtDecode(jwt);
  } catch (ex) {
    return null;
  }
}

export function loginWithJwt(jwt) {
  localStorage.setItem(tokenKey, jwt);
}

export function getJwt() {
  return localStorage.getItem(tokenKey);
}

export default {
  login,
  logout,
  getCurrentUser,
  loginWithJwt,
  getJwt,
};
