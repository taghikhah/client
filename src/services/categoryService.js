// Axios
import http from "./httpService";

// Constants
const apiEndpoint = "/category";
const tokenKey = process.env.REACT_APP_TOKEN_KEY;

// Get the current logged-in-user Token from the Browser
const jwt = localStorage.getItem(tokenKey);

// Define and get API routes
function categoryUrl(id) {
  return `${apiEndpoint}/${id}`;
}

// API request to get all of the Categories
export function getCategories() {
  return http.get(apiEndpoint);
}

// API request to get the maximum Priority
export function getCategoriesMaxPriority() {
  return http.get(`${apiEndpoint}/priority`);
}

// API request to get a Category by ID
export function getCategory(category_id) {
  return http.get(categoryUrl(category_id));
}

// API request to create a new Category or update an existing Category by ID
export function saveCategory(category) {
  if (category._id) {
    const body = { ...category };
    delete body._id;
    return http.put(categoryUrl(category._id), body);
  }

  return http.post(apiEndpoint, category);
}

// API request to delete a Category by ID
export function deleteCategory(category_id) {
  return http.delete(categoryUrl(category_id), {
    headers: { "x-auth-token": jwt },
  });
}
