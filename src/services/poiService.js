// Axios
import http from "./httpService";

// Constants
const apiEndpoint = "/poi";
const tokenKey = process.env.REACT_APP_TOKEN_KEY;

// Get the current logged-in-user Token from the Browser
const jwt = localStorage.getItem(tokenKey);

// Define and get API routes
function poiUrl(id) {
  return `${apiEndpoint}/${id}`;
}

// API request to get all of the Pois
export function getPois() {
  return http.get(apiEndpoint);
}

// API request to get the maximum Priority
export function getPoisMaxPriority() {
  return http.get(`${apiEndpoint}/priority`);
}

// API request to get a Poi by ID
export function getPoi(poi_id) {
  return http.get(poiUrl(poi_id));
}

// API request to create a new Poi or update an existing Poi by ID
export function savePoi(poi) {
  if (poi._id) {
    const body = { ...poi };
    delete body._id;
    return http.put(poiUrl(poi._id), body);
  }

  return http.post(apiEndpoint, poi);
}

// API request to delete a Poi by ID
export function deletePoi(poi_id) {
  return http.delete(poiUrl(poi_id), { headers: { "x-auth-token": jwt } });
}
