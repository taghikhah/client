// Libraries
import React from "react";
import PropTypes from "prop-types";
import { Link, NavLink } from "react-router-dom";

const NavBar = ({ user }) => {
  if (window.location.pathname.startsWith("/gallery")) {
    return null;
  } else {
    return (
      <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
        <Link className="navbar-brand" to="/">
          <img
            src={`${process.env.PUBLIC_URL}/logo512.png`}
            width="30"
            height="30"
            className="d-inline-block align-top mr-2"
            alt="Histocaching"
          />
          <span className="big bold">Histocaching</span>{" "}
        </Link>
        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarNavAltMarkup"
          aria-controls="navbarNavAltMarkup"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>

        <div
          className="collapse navbar-collapse navbar-dark bg-dark"
          id="navbarNavAltMarkup"
        >
          <ul className="navbar-nav ml-auto mr-2 my-3 my-lg-0">
            {user && (
              <li className="nav-item">
                <NavLink className="nav-link" to="/dashboard">
                  Dashboard
                </NavLink>
              </li>
            )}
          </ul>

          {!user && (
            <NavLink to="/login">
              <button
                className="btn btn-outline-warning rounded-pill px-3 mb-2 mb-lg-0"
                data-bs-toggle="modal"
              >
                <span className="d-flex align-items-center">
                  <i className="bx bx-log-in-circle"></i>{" "}
                  <span className="big ml-2">Login</span>
                </span>
              </button>
            </NavLink>
          )}
          {user && (
            <NavLink to="/logout">
              <button
                className="btn btn-outline-warning rounded-pill px-3 mb-2 mb-lg-0"
                data-bs-toggle="modal"
              >
                <span className="d-flex align-items-center ">
                  <i className="bx bx-log-out-circle"></i>{" "}
                  <span className="big ml-2">Logout</span>
                </span>
              </button>
            </NavLink>
          )}
        </div>
      </nav>
    );
  }
};

// Typechecking
NavBar.propTypes = {
  user: PropTypes.string.isRequired,
};

export default NavBar;
