// Libraries
import React from "react";
import { Switch, Redirect } from "react-router-dom";

// Child Components
import Public from "../../routes/public";
import NavBar from "./navBar";
import Home from "./home";
import Imprint from "./imprint";
import PrivacyPolicy from "./privacyPolicy";
import Gallery from "./gallery";
import NotFound from "./notFound";
import Register from "./register";
import Login from "./login";
import Logout from "../users/logout";

// Functions
import authService from "../../services/authService";

class Landing extends React.Component {
  state = { user: "" };

  componentDidMount() {
    const user = authService.getCurrentUser();
    this.setState({ user });
  }

  render() {
    return (
      <React.Fragment>
        <NavBar user={this.state.user} />
        <Switch>
          <Public path="/register" component={Register} />
          <Public path="/login" component={Login} />
          <Public path="/logout" component={Logout} />
          <Public path="/not-found" component={NotFound} />
          <Redirect from="/dashboard" exact to="/login" />
          <Public path="/imprint" component={Imprint} />
          <Public path="/privacy-policy" component={PrivacyPolicy} />
          <Public path="/gallery" component={Gallery} />
          <Public path="/" component={Home} />
          <Redirect to="/not-found" />
        </Switch>
      </React.Fragment>
    );
  }
}

export default Landing;
