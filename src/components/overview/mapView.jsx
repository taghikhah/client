// Libraries
import React, { Component } from "react";
import GoogleMapReact from "google-map-react";

// Config
import { mapStyle } from "../../utils/scripts/map";

// Constants
const googleMapKey = process.env.REACT_APP_GOOGLE_MAP_KEY;

class MapView extends Component {
  render() {
    function createMapOptions(maps) {
      return {
        zoomControlOptions: {
          position: maps.ControlPosition.RIGHT_BOTTOM,
        },
        streetViewControlOptions: {
          position: maps.ControlPosition.RIGHT_TOP,
        },
        mapTypeControl: false,
        streetViewControl: true,
        styles: mapStyle,
        rotateControl: true,
        fullscreenControl: false,
        scrollwheel: true,
        disableDoubleClickZoom: true,
      };
    }

    return (
      <div style={{ height: "50vh", width: "100%" }}>
        <GoogleMapReact
          bootstrapURLKeys={{
            key: googleMapKey,
          }}
          defaultCenter={[51.028139, 13.731214]}
          defaultZoom={16}
          options={createMapOptions}
        >
          {this.props.pois.map((p) => (
            <div
              className="map-tick"
              key={p._id}
              lat={p.lat}
              lng={p.long}
              onClick={this.props.onClick}
              onMouseDown={this.props.onMouseDown}
            >
              <div className="map-box" key={p.priority}>
                {p.name}
              </div>
            </div>
          ))}
        </GoogleMapReact>
      </div>
    );
  }
}

export default MapView;
