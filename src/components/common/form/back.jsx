// Libraries
import React from "react";
import PropTypes from "prop-types";

const Back = ({ onClick }) => {
  return (
    <button onClick={onClick} className="btn btn-primary rounded">
      <i className="fa fa-chevron-left" aria-hidden="true"></i> Back
    </button>
  );
};

// Typechecking
Back.propTypes = {
  onClick: PropTypes.func.isRequired,
};

export default Back;
