// Libraries
import React from "react";
import PropTypes from "prop-types";

const Search = ({ value, onChange }) => {
  return (
    <React.Fragment>
      <input
        className="form-control mr-sm-2"
        type="search"
        name="query"
        placeholder="Search..."
        aria-label="Search"
        value={value}
        onChange={(e) => onChange(e.currentTarget.value)}
        style={{
          borderRadius: "50px",
        }}
      ></input>
    </React.Fragment>
  );
};

// Typechecking
Search.propTypes = {
  onChange: PropTypes.func.isRequired,
};

export default Search;
