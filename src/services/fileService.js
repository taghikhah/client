// Axios
import http from "./commonService";

// Constants
const apiEndpoint = "/files";

// Upload a File or Files
export function uploadFiles(file, header, onUploadProgress) {
  let formData = new FormData();

  formData.append("file", file);

  return http.post(`${apiEndpoint}/`, formData, {
    headers: {
      "x-folder-name": header,
      "Content-Type": "multipart/form-data",
    },
    onUploadProgress,
  });
}

// Get list of Files
export function getFiles() {
  return http.get(`${apiEndpoint}/`);
}

// Download a File by name
export function getFile(file_name) {
  return http.get(`${apiEndpoint}/${file_name}`);
}

// Delete a File by name
export function deleteFile(file_name) {
  return http.delete(`${apiEndpoint}/${file_name}`);
}

// Delete a Directory by ID
export function deleteDirectory(id) {
  return http.delete(`${apiEndpoint}/directory/${id}`);
}
