// Libraries
import React, { Component } from "react";
import PropTypes from "prop-types";

// Child Components
import Table from "../common/table";
import Actions from "../common/table/actions";
import Role from "../common/misc/role";
import Format from "../common/misc/format";

class UsersTable extends Component {
  columns = [
    { path: "name", label: "Name", isSortable: true },
    {
      path: "email",
      label: "Email",
      isSortable: true,
      content: (user) => <a href={`mailto:${user.email}`}>{user.email}</a>,
    },
    {
      path: "role",
      label: "Role",
      isSortable: false,
      content: (user) => <Role role={user.role} />,
    },
    {
      path: "created_at",
      label: "Created",
      isSortable: true,
      content: (user) => <Format date={user.created_at} />,
    },
    {
      key: "actions",
      label: "Actions",
      isSortable: false,
      content: (user) => (
        <Actions
          onClick={() => this.props.onDelete(user)}
          path="user"
          item={user}
        />
      ),
    },
  ];

  render() {
    const { users, startNumber, onSort, sortColumn } = this.props;
    return (
      <Table
        columns={this.columns}
        data={users}
        sortColumn={sortColumn}
        onSort={onSort}
        startNumber={startNumber}
      />
    );
  }
}

// Typechecking
UsersTable.propTypes = {
  users: PropTypes.array.isRequired,
  startNumber: PropTypes.number.isRequired,
  onSort: PropTypes.func.isRequired,
  sortColumn: PropTypes.object.isRequired,
};

export default UsersTable;
