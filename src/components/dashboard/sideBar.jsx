// Libraries
import React from "react";
import PropTypes from "prop-types";
import { Link, NavLink } from "react-router-dom";

const SideBar = ({ role }) => {
  return (
    <div className="l-navbar" id="nav-bar">
      <nav className="nav">
        <div>
          {" "}
          <Link to="#" className="nav_logo">
            {" "}
            <img
              src={`${process.env.PUBLIC_URL}/logo192.png`}
              width="35"
              height="35"
              className="d-inline-block nav_logo-icon"
              alt="Histocaching"
            />{" "}
            <span className="nav_logo-name">Histocaching</span>{" "}
          </Link>
          <div className="nav_list">
            {" "}
            <NavLink
              title="Overview"
              className="nav_link"
              to="/dashboard/overview"
            >
              {" "}
              <i className="bx bx-layer nav_icon"></i>{" "}
              <span className="nav_name">Overview</span>{" "}
            </NavLink>{" "}
            {role !== "editor" && (
              <React.Fragment>
                <NavLink
                  title="Users"
                  className="nav_link"
                  to="/dashboard/users"
                >
                  {" "}
                  <i className="bx bxs-group nav_icon"></i>{" "}
                  <span className="nav_name">Users</span>{" "}
                </NavLink>{" "}
              </React.Fragment>
            )}
            <NavLink
              title="Categories"
              className="nav_link"
              to="/dashboard/categories"
            >
              {" "}
              <i className="bx bx-category-alt nav_icon"></i>{" "}
              <span className="nav_name">Categories</span>{" "}
            </NavLink>{" "}
            <NavLink
              title="Histocaches"
              className="nav_link"
              to="/dashboard/pois"
            >
              {" "}
              <i className="bx bx-map-pin nav_icon"></i>{" "}
              <span className="nav_name">Histocaches</span>{" "}
            </NavLink>{" "}
            <NavLink title="Items" className="nav_link" to="/dashboard/items">
              {" "}
              <i className="bx bx-windows nav_icon"></i>{" "}
              <span className="nav_name">Items</span>{" "}
            </NavLink>{" "}
          </div>
        </div>{" "}
        <Link title="Logout" className="nav_link" to="/logout">
          {" "}
          <i className="bx bx-log-out nav_icon"></i>{" "}
          <span className="nav_name">SignOut</span>{" "}
        </Link>
      </nav>
    </div>
  );
};

// Typechecking
SideBar.propTypes = {
  role: PropTypes.string.isRequired,
};

export default SideBar;
