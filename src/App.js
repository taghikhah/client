// Libraries
import React, { Component } from "react";
import { ToastContainer } from "react-toastify";

// Child Component
import Routes from "./routes/routes";

// Built-in CSS Files
import "font-awesome/css/font-awesome.css";
import "boxicons/css/boxicons.min.css";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.bundle";
import "react-toastify/dist/ReactToastify.css";

// Run-time CSS Files
import "./utils/styles/index.css";
import "./utils/styles/dashboard.css";
import "./utils/styles/preloader.css";
import "./utils/styles/lightbox.css";
import "./utils/styles/landing.css";

class App extends Component {
  render() {
    return (
      <React.Fragment>
        <ToastContainer
          position="top-right"
          autoClose={5000}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick={false}
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
        />
        <Routes />
      </React.Fragment>
    );
  }
}

export default App;
