// Libraries
import React, { Component } from "react";

// Child Component
import Footer from "./footer";

class Home extends Component {
  render() {
    const url = window.location.search;
    if (url === "") {
      return (
        <div className="">
          <header className="masthead">
            <div className="container px-5">
              <div className="row gx-5 align-items-center">
                <div className="col-lg-6">
                  <div className="mb-5 mb-lg-0 text-center text-lg-start">
                    <h1 className="display-1 mb-3">
                      Explore Dresden's History.
                    </h1>
                    <p className="lead fw-normal text-muted mb-5">
                      Download the mobile app and you are good to go.
                      <br />
                      Enjoy caching the history of Dresden!
                    </p>
                    <div className="d-flex flex-column flex-lg-row align-items-center">
                      <a
                        className="mr-lg-3 mb-4 mb-lg-0"
                        href="https://play.google.com/store/apps/details?id=org.histocaching.geheim"
                        target="_blank"
                      >
                        <img
                          style={{ height: "3rem" }}
                          src={`${process.env.PUBLIC_URL}/badges/google-play-badge.svg`}
                          alt="..."
                        />
                      </a>
                      <a
                        href="https://apps.apple.com/us/app/geheim-stasi-ander-tu-dresden/id1600862955"
                        target="_blank"
                      >
                        <img
                          style={{ height: "3rem" }}
                          src={`${process.env.PUBLIC_URL}/badges/app-store-badge.svg`}
                          alt="..."
                        />
                      </a>
                    </div>
                  </div>
                </div>
                <div className="col-lg-6">
                  <div className="masthead-device-mockup">
                    <svg
                      className="circle"
                      viewBox="0 0 100 100"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <defs>
                        <linearGradient
                          id="circleGradient"
                          gradientTransform="rotate(45)"
                        >
                          <stop
                            className="gradient-start-color"
                            offset="0%"
                          ></stop>
                          <stop
                            className="gradient-end-color"
                            offset="100%"
                          ></stop>
                        </linearGradient>
                      </defs>
                      <circle cx="50" cy="50" r="50"></circle>
                    </svg>
                    <svg
                      className="shape-1 d-none d-sm-block"
                      viewBox="0 0 240.83 240.83"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <rect
                        x="-32.54"
                        y="78.39"
                        width="305.92"
                        height="84.05"
                        rx="42.03"
                        transform="translate(120.42 -49.88) rotate(45)"
                      ></rect>
                      <rect
                        x="-32.54"
                        y="78.39"
                        width="305.92"
                        height="84.05"
                        rx="42.03"
                        transform="translate(-49.88 120.42) rotate(-45)"
                      ></rect>
                    </svg>
                    <svg
                      className="shape-2 d-none d-sm-block"
                      viewBox="0 0 100 100"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <circle cx="50" cy="50" r="50"></circle>
                    </svg>
                    <div className="device-wrapper">
                      <div
                        className="device"
                        data-device="iPhoneX"
                        data-orientation="portrait"
                        data-color="black"
                      >
                        <div className="screen bg-black">
                          <video
                            muted="muted"
                            autoPlay={true}
                            loop={true}
                            style={{ maxWidth: "100%", height: "100%" }}
                          >
                            <source
                              src={`${process.env.PUBLIC_URL}/home/demo.mov`}
                              type="video/mp4"
                            />
                          </video>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </header>
          <aside className="text-center bg-gradient-primary-to-secondary pt-5">
            <div className="container">
              <div className="row justify-content-center">
                <div className="col">
                  <div className="h5 text-white my-4 py-5">
                    <div className="row align-items-center d-flex justify-content-center">
                      <div className="col">
                        <p className="text-justify">
                          The app “Geheim!“ is a virtual tour on the campus of
                          TU Dresden. It enables users to visit various places
                          that were monitored, investigated and surveilled by
                          Stasi as late as 1989. These places and their stories
                          can be explored by finding histocaches.
                        </p>
                      </div>
                    </div>
                    <div className="row align-items-center justify-content-center">
                      <div className="col-sm-6">
                        <video
                          autoPlay={false}
                          controls={true}
                          style={{ maxWidth: "100%", height: "100%" }}
                          poster={`${process.env.PUBLIC_URL}/home/about.jpg`}
                        >
                          <source
                            src={`${process.env.PUBLIC_URL}/home/about.mp4`}
                            type="video/mp4"
                          />
                        </video>
                      </div>
                      <div className="col-sm-6">
                        <p className="text-justify">
                          The histocaches contain stories from the files of the
                          Stasi, associated with buildings and places on the
                          campus of TU Dresden. The app provides insights into
                          these files ‒ outside the Archives, at the scene where
                          they happened. Augmented reality revives the historic
                          views of these places.
                        </p>
                        <p className="text-justify">
                          The stories tell of the monitoring and protection of
                          sensitive research areas like nuclear physics, but
                          also the surveillance of everyday student life in
                          dormitories and clubs. They can highlight only small
                          parts of the Stasi surveillance system, which kept
                          evolving over the 40 years of existence of the GDR.
                        </p>
                      </div>
                    </div>
                    <div className="row align-items-center d-flex justify-content-center">
                      <div className="col">
                        <p className="text-justify">
                          "Geheim!" is a joint project of the Federal Archives
                          and the Stasi Records Archive, the Chair of Computer
                          Graphics and Visualization at TU Dresden and the
                          University Archive of the TU Dresden.
                        </p>
                        <p className="text-justify">
                          The app was developed by students, doing a complex lab
                          project at the Chair of Computer Graphics and
                          Visualization. Research and curating was done by the
                          Stasi Records Archive Dresden. The University Archive
                          Dresden contributed the historic photographs used in
                          AR mode as well as additional background information.
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </aside>
          <Footer />
        </div>
      );
    } else {
      return (
        <div className="">
          <header className="masthead">
            <div className="container px-5">
              <div className="row gx-5 align-items-center">
                <div className="col-lg-6">
                  <div className="mb-5 mb-lg-0 text-center text-lg-start">
                    <h1 className="display-1 mb-3">
                      Entdecken Sie Dresdens Geschichte.
                    </h1>
                    <p className="lead fw-normal text-muted mb-5">
                      Laden Sie die mobile App herunter und schon kann es
                      losgehen.
                      <br />
                      Viel Spaß beim Cachen der Geschichte von Dresden!
                    </p>
                    <div className="d-flex flex-column flex-lg-row align-items-center">
                      <a
                        className="mr-lg-3 mb-4 mb-lg-0"
                        href="https://play.google.com/store/apps/details?id=org.histocaching.geheim"
                        target="_blank"
                      >
                        <img
                          style={{ height: "3rem" }}
                          src={`${process.env.PUBLIC_URL}/badges/google-play-badge.svg`}
                          alt="..."
                        />
                      </a>
                      <a
                        href="https://apps.apple.com/us/app/geheim-stasi-ander-tu-dresden/id1600862955"
                        target="_blank"
                      >
                        <img
                          style={{ height: "3rem" }}
                          src={`${process.env.PUBLIC_URL}/badges/app-store-badge.svg`}
                          alt="..."
                        />
                      </a>
                    </div>
                  </div>
                </div>
                <div className="col-lg-6">
                  <div className="masthead-device-mockup">
                    <svg
                      className="circle"
                      viewBox="0 0 100 100"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <defs>
                        <linearGradient
                          id="circleGradient"
                          gradientTransform="rotate(45)"
                        >
                          <stop
                            className="gradient-start-color"
                            offset="0%"
                          ></stop>
                          <stop
                            className="gradient-end-color"
                            offset="100%"
                          ></stop>
                        </linearGradient>
                      </defs>
                      <circle cx="50" cy="50" r="50"></circle>
                    </svg>
                    <svg
                      className="shape-1 d-none d-sm-block"
                      viewBox="0 0 240.83 240.83"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <rect
                        x="-32.54"
                        y="78.39"
                        width="305.92"
                        height="84.05"
                        rx="42.03"
                        transform="translate(120.42 -49.88) rotate(45)"
                      ></rect>
                      <rect
                        x="-32.54"
                        y="78.39"
                        width="305.92"
                        height="84.05"
                        rx="42.03"
                        transform="translate(-49.88 120.42) rotate(-45)"
                      ></rect>
                    </svg>
                    <svg
                      className="shape-2 d-none d-sm-block"
                      viewBox="0 0 100 100"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <circle cx="50" cy="50" r="50"></circle>
                    </svg>
                    <div className="device-wrapper">
                      <div
                        className="device"
                        data-device="iPhoneX"
                        data-orientation="portrait"
                        data-color="black"
                      >
                        <div className="screen bg-black">
                          <video
                            muted="muted"
                            autoPlay={true}
                            loop={true}
                            style={{ maxWidth: "100%", height: "100%" }}
                          >
                            <source
                              src={`${process.env.PUBLIC_URL}/home/demo.mov`}
                              type="video/mp4"
                            />
                          </video>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </header>
          <aside className="text-center bg-gradient-primary-to-secondary pt-5">
            <div className="container">
              <div className="row justify-content-center">
                <div className="col">
                  <div className="h5 text-white my-4 py-5">
                    <div className="row align-items-center d-flex justify-content-center">
                      <div className="col">
                        <p className="text-justify">
                          Die App „Geheim!“ ist eine virtuelle Spurensuche auf
                          dem Campus der TU Dresden. Sie führt zu verschiedenen
                          Orten, an denen die Stasi bis 1989 beobachtete,
                          überwachte und ermittelte. Über Histocaches können
                          diese Orte und Geschichten entdeckt werden.
                        </p>
                      </div>
                    </div>
                    <div className="row align-items-center justify-content-center">
                      <div className="col-sm-6">
                        <video
                          autoPlay={false}
                          controls={true}
                          style={{ maxWidth: "100%", height: "100%" }}
                          poster={`${process.env.PUBLIC_URL}/home/about.jpg`}
                        >
                          <source
                            src={`${process.env.PUBLIC_URL}/home/about.mp4`}
                            type="video/mp4"
                          />
                        </video>
                      </div>
                      <div className="col-sm-6">
                        <p className="text-justify">
                          Es sind Geschichten aus den Stasi-Akten, die mit
                          Gebäuden auf dem Campus verknüpft sind. Die App
                          gewährt einen Einblick in diese Geheimdienstakten –
                          außerhalb des Archivs, am Ort des Geschehens.
                          Augmented Reality (AR) lässt die historischen Gebäude
                          wieder auferstehen.
                        </p>
                        <p className="text-justify">
                          Die Geschichten erzählen von der Überwachung und
                          Absicherung sensibler Forschungsbereiche, z.B. der
                          Kernforschung, ebenso wie vom studentischen
                          Alltagsleben, welches sich in Wohnheimen und
                          Studentenklubs abspielte. Es sind Schlaglichter, die
                          nur kleine Ausschnitte des Stasi-Überwachungssytems
                          zeigen können, welches sich über die 40 Jahre, in der
                          die DDR bestand, auch selbst veränderte.
                        </p>
                      </div>
                    </div>
                    <div className="row align-items-center d-flex justify-content-center">
                      <div className="col">
                        <p className="text-justify">
                          Die App „Geheim!“ ist ein Kooperationsprojekt des
                          Bundesarchivs/Stasi-Unterlagen-Archivs, der Professur
                          für Computergraphik und Visualisierung der TU Dresden
                          und dem Universitätsarchiv der TU Dresden.
                        </p>
                        <p className="text-justify">
                          Programmiert und erstellt wurde die App von
                          Studierenden im Rahmen eines Komplexpraktikums,
                          welches die Professur für Computergraphik und
                          Visualisierung zum Thema „HistoCaching II“ im
                          Sommersemester 2021 anbot. Die Recherche und Texte der
                          App sowie die Auswahl der Dokumente übernahm das
                          Stasi-Unterlagen-Archiv Dresden, das
                          Universitätsarchiv Dresden steuerte historische
                          Bildaufnahmen für die AR-Ansichten und
                          Hintergrundinformationen bei.
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </aside>
          <Footer />
        </div>
      );
    }
  }
}

export default Home;
