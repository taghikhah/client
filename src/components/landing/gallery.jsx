// Libraries
import React, { Component } from "react";

class Gallery extends Component {
  render() {
    const url = window.location.search;

    if (url === "") {
      return (
        <div style={{ backgroundColor: "black" }}>
          <div className="col page">
            <div
              className="row justify-content-between align-items-center"
              style={{ color: "#fec000" }}
            >
              <div className="col-md-10 mt-2">
                <h2>Georg Schumann Building (Münchner Platz Memorial)</h2>
              </div>
            </div>
            <hr className="mb-4" style={{ borderTop: "solid 3px #fec000" }} />
            <div className="row justify-content-between align-items-center">
              <div className="col-sm-6">
                <video
                  autoPlay={false}
                  controls={true}
                  style={{ maxWidth: "100%", height: "100%" }}
                >
                  <source
                    src={`${process.env.PUBLIC_URL}/videos/Ausschnitte-v1.mp4`}
                    type="video/mp4"
                  />
                </video>
              </div>
              <div className="col-sm-6">
                <video
                  autoPlay={false}
                  controls={true}
                  style={{ maxWidth: "100%", height: "100%" }}
                >
                  <source
                    src={`${process.env.PUBLIC_URL}/videos/Ausschnitte-v2.mp4`}
                    type="video/mp4"
                  />
                </video>
              </div>
            </div>

            <div className="row justify-content-between align-items-center">
              <div className="col-sm-6">
                <video
                  autoPlay={false}
                  controls={true}
                  style={{ maxWidth: "100%", height: "100%" }}
                >
                  <source
                    src={`${process.env.PUBLIC_URL}/videos/Ausschnitte-v3.mp4`}
                    type="video/mp4"
                  />
                </video>
              </div>
              <div className="col-sm-6">
                <video
                  autoPlay={false}
                  controls={true}
                  style={{ maxWidth: "100%", height: "100%" }}
                >
                  <source
                    src={`${process.env.PUBLIC_URL}/videos/Ausschnitte-v4.mp4`}
                    type="video/mp4"
                  />
                </video>
              </div>
            </div>
            <p
              className="text-center mt-3"
              style={{ color: "grey", fontSize: "0.7rem" }}
            >
              Excerpts from the DEFA documentary by Roza Berger-Fiedler, “Im
              Namen des Volkes” (1989), Stasi recording (BArch, MfS, HA XXII, Vi
              32, Copyright: Bundesfilmarchiv)
            </p>

            <div
              className="row justify-content-center mt-5"
              style={{ color: "white" }}
            >
              <div className="col">
                <p className="text-justify">
                  "In the name of the people," after 1945 Nazi criminals were
                  put on highly publicised trials at Münchner Platz.
                </p>
                <p className="text-justify">
                  The DEFA documentary of the same name focuses on the "jurists'
                  trial." It shows the rigorous sentencing of convicted jurists.
                  The anti-fascism propagated in the trials became part of the
                  founding myth of the young GDR and central to its self-image.
                </p>
              </div>
            </div>
          </div>
        </div>
      );
    } else {
      return (
        <div style={{ backgroundColor: "black" }}>
          <div className="col page">
            <div
              className="row justify-content-between align-items-center"
              style={{ color: "#fec000" }}
            >
              <div className="col-md-10 mt-2">
                <h2>Georg-Schumann-Bau (Gedenkstätte Münchner Platz)</h2>
              </div>
            </div>
            <hr className="mb-4" style={{ borderTop: "solid 3px #fec000" }} />
            <div className="row justify-content-between align-items-center">
              <div className="col-sm-6">
                <video
                  autoPlay={false}
                  controls={true}
                  style={{ maxWidth: "100%", height: "100%" }}
                >
                  <source
                    src={`${process.env.PUBLIC_URL}/videos/Ausschnitte-v1.mp4`}
                    type="video/mp4"
                  />
                </video>
              </div>
              <div className="col-sm-6">
                <video
                  autoPlay={false}
                  controls={true}
                  style={{ maxWidth: "100%", height: "100%" }}
                >
                  <source
                    src={`${process.env.PUBLIC_URL}/videos/Ausschnitte-v2.mp4`}
                    type="video/mp4"
                  />
                </video>
              </div>
            </div>

            <div className="row justify-content-between align-items-center">
              <div className="col-sm-6">
                <video
                  autoPlay={false}
                  controls={true}
                  style={{ maxWidth: "100%", height: "100%" }}
                >
                  <source
                    src={`${process.env.PUBLIC_URL}/videos/Ausschnitte-v3.mp4`}
                    type="video/mp4"
                  />
                </video>
              </div>
              <div className="col-sm-6">
                <video
                  autoPlay={false}
                  controls={true}
                  style={{ maxWidth: "100%", height: "100%" }}
                >
                  <source
                    src={`${process.env.PUBLIC_URL}/videos/Ausschnitte-v4.mp4`}
                    type="video/mp4"
                  />
                </video>
              </div>
            </div>
            <p
              className="text-center mt-3"
              style={{ color: "grey", fontSize: "0.7rem" }}
            >
              Ausschnitte aus dem DEFA-Dokumentarfilm von Roza Berger-Fiedler
              „Im Namen des Volkes“ (1989), Stasi-Mitschnitt (BArch, MfS, HA
              XXII, Vi 32, Rechte: Bundesfilmarchiv)
            </p>

            <div
              className="row justify-content-center mt-5"
              style={{ color: "white" }}
            >
              <div className="col">
                <p className="text-justify">
                  "Im Namen des Volkes" wurde nach 1945 am Münchner Platz
                  "Nazi-Verbrechern" öffentlichkeitswirksam der Prozess gemacht.
                </p>
                <p className="text-justify">
                  Die gleichnamige DEFA-Dokumentation geht auf den
                  "Juristen-Prozess" ein. Die rigorose Aburteilung von
                  belasteten Juristen wird demonstriert. Der in den Prozessen
                  propagierte Antifaschismus wurde zum Gründungsmythos und
                  zentral für das Selbstverständnis der jungen DDR.
                </p>
              </div>
            </div>
          </div>
        </div>
      );
    }
  }
}

export default Gallery;
