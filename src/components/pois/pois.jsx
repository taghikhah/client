// Libraries
import React, { Component } from "react";
import { Link } from "react-router-dom";
import { toast } from "react-toastify";
import _ from "lodash";

// Common Components
import Search from "../common/search";
import Filter from "../common/filter";
import TableMeta from "../common/table/tableMeta";
import Pagination from "../common/pagination";

// Child Components
import PoisTable from "./poisTable";
import Expire from "../common/expire";

// Functions
import { paginate } from "../../utils/scripts/paginate";
import { getPois, deletePoi } from "../../services/poiService";
import { getCategories } from "../../services/categoryService";
import { deleteDirectory } from "../../services/fileService";
import { getItems } from "../../services/itemService";

class Pois extends Component {
  state = {
    pois: [],
    categories: [],
    currentPage: 1,
    initialSize: 3,
    pageSize: 3,
    searchQuery: "",
    selectedCategory: null,
    sortColumn: { path: "title_en", order: "asc" },
  };

  // Retrive the Data from the Server
  async componentDidMount() {
    // Get Categories and Modify
    const { data: allCategories } = await getCategories();
    if (allCategories.length === 0) return;

    // When at least a Poi exists in the database:
    const data = allCategories.map(({ name_en: name, ...rest }) => ({
      name,
      ...rest,
    }));
    const categories = [{ _id: "", name: "All Categories" }, ...data];

    // Find related Category
    function getGategory(id) {
      var category = allCategories.find((c) => {
        return c._id === id;
      });
      if (category) return category;
    }

    // Get Pois and Modify
    const { data: allPois } = await getPois();

    const pois = allPois.map(({ category_id: category, ...rest }) => ({
      category: {
        _id: getGategory(category)._id,
        name: getGategory(category).name_en,
        name_de: getGategory(category).name_de,
      },

      ...rest,
    }));

    this.setState({ pois, categories });
  }

  // Delete a Poi by ID
  handleDelete = async (poi) => {
    // Delete Validation
    const { data: items } = await getItems();
    const result = items.filter((i) => i.poi_id === poi._id);
    if (result.length !== 0) {
      toast.error(
        <div>
          Not allowed!
          <br /> First, delete the {result.length} Item(s) of this Histocache.
        </div>
      );
      return;
    }

    // Delete Poi from the State
    const originalPois = this.state.pois;
    const pois = originalPois.filter((p) => p._id !== poi._id);

    this.setState({ pois });

    try {
      const response = await deletePoi(poi._id);
      if (response.data && response.status === 200) {
        toast.success(
          <div>
            Histocache: "{response.data.title_en}" <br /> deleted successfully.
          </div>
        );

        //this.deleteFolder(poi._id);
        await deleteDirectory(poi._id);
      }
    } catch (ex) {
      if (ex.response && ex.response.status === 401) {
        toast.error("Only Admins can delete a Histocache!");
      } else if (ex.response && ex.response.status === 404) {
        toast.error("This Histocache has already been deleted.");
      }

      this.setState({ pois: originalPois });
    }
  };

  // Pagination Event Handler
  handlePageChange = (page) => {
    this.setState({ currentPage: page });
  };

  // PageSize Event Handler
  handleSizeChange = ({ currentTarget: input }) => {
    let pageSize = { ...this.state.pageSize };
    pageSize = input.value;

    this.setState({ pageSize });
  };

  // Filter Event Handler
  handleCategorySelect = ({ currentTarget: input }) => {
    const category = this.state.categories.find((c) => c._id === input.value);

    this.setState({
      selectedCategory: category,
      searchQuery: "",
      currentPage: 1,
    });
  };

  // Search Event Handler
  handleSearch = (query) => {
    this.setState({
      searchQuery: query,
      selectedCategory: null,
      currentPage: 1,
    });
  };

  // Sort Event Handler
  handleSort = (sortColumn) => {
    this.setState({ sortColumn });
  };

  // Get state Data
  getData = () => {
    const {
      currentPage,
      pageSize,
      pois: allPois,
      selectedCategory,
      searchQuery,
      sortColumn,
    } = this.state;

    // Calculate StartNumber
    const startNumber = (currentPage - 1) * pageSize;

    // Get Filter Options
    let filtered = allPois;
    if (searchQuery)
      filtered = allPois.filter(
        (p) =>
          p.title_en.toLowerCase().indexOf(searchQuery.toLowerCase()) !== -1 ||
          p.title_de.toLowerCase().indexOf(searchQuery.toLowerCase()) !== -1
      );
    else if (selectedCategory && selectedCategory._id)
      filtered = allPois.filter((p) => p.category._id === selectedCategory._id);

    // Define Sorting Option
    const sorted = _.orderBy(filtered, [sortColumn.path], [sortColumn.order]);

    // Define Pagination
    const pois = paginate(sorted, currentPage, pageSize);

    return {
      startNumber: startNumber,
      totalCount: filtered.length,
      data: pois,
    };
  };

  render() {
    const { length: count } = this.state.pois;
    const {
      currentPage,
      pageSize,
      sortColumn,
      searchQuery,
      categories,
      initialSize,
    } = this.state;
    const { startNumber, totalCount, data: pois } = this.getData();

    return (
      <div className="col alt-table-responsive mt-1">
        <div className="row justify-content-between align-items-center underline my-2">
          <div className="col-md-6 mt-2">
            <h2>
              <i className="bx bx-map-pin"></i> Histocaches
            </h2>
          </div>
          <div className="col-sm-12 col-md-5 col-lg-4 col-xl-3 mb-2 text-end">
            {categories.length === 0 && (
              <Link
                className="btn btn-outline-dark btn-block rounded"
                to="/dashboard/categories/"
              >
                <i className="fa fa-exclamation-circle" aria-hidden="true"></i>{" "}
                First create a new Category{" "}
              </Link>
            )}
            {categories.length !== 0 && (
              <Link
                className="btn btn-warning btn-block rounded"
                to="/dashboard/poi/new"
              >
                <i className="fa fa-plus-circle" aria-hidden="true"></i> Create
                a new Histocache
              </Link>
            )}
          </div>
        </div>
        {count !== 0 && (
          <React.Fragment>
            <div className="row justify-content-between align-items-center pb-1">
              <div className="col-md-6 my-2">
                <Filter
                  name={"dropdown"}
                  label={"Filter"}
                  options={this.state.categories}
                  selectedOption={this.state.selectedCategory}
                  onChange={this.handleCategorySelect}
                />
              </div>
              <div className="col-sm-12 col-md-5 col-lg-4 col-xl-3 my-2 text-end">
                <Search value={searchQuery} onChange={this.handleSearch} />
              </div>
            </div>

            <div className="row outer">
              <div className="col">
                <PoisTable
                  pois={pois}
                  sortColumn={sortColumn}
                  startNumber={startNumber}
                  onLike={this.handleLike}
                  onDelete={this.handleDelete}
                  onSort={this.handleSort}
                />
              </div>
            </div>
          </React.Fragment>
        )}

        {count === 0 && (
          <div className="row">
            <div className="col-sm-12 align-self-center">
              <Expire delay={5000}>
                <div className="loader">
                  <div></div>
                  <div></div>
                  <div></div>
                </div>
              </Expire>
            </div>
          </div>
        )}

        <hr />
        <div className="row align-items-center">
          <div className="col-sm-12 col-md col-lg col-xl mr-auto pb-1">
            <Pagination
              itemsCount={totalCount}
              pageSize={pageSize}
              currentPage={currentPage}
              onPageChange={this.handlePageChange}
            />
          </div>
          <div className="col-auto align-middle">
            <TableMeta
              initial={initialSize}
              itemsCount={count}
              total={totalCount}
              onChange={this.handleSizeChange}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default Pois;
