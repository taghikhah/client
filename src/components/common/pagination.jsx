// Libraries
import React from "react";
import PropTypes from "prop-types";
import _ from "lodash";

const Pagination = ({ itemsCount, pageSize, onPageChange, currentPage }) => {
  const pagesCount = Math.ceil(itemsCount / pageSize);

  if (pagesCount === 1) return null;

  let startPage, endPage;
  if (pagesCount <= 4) {
    // less than 4 total pages so show all
    startPage = 1;
    endPage = pagesCount;
  } else {
    // more than 4 total pages so calculate start and end pages
    if (currentPage <= 3) {
      startPage = 1;
      endPage = 4;
    } else if (currentPage + 1 >= pagesCount) {
      startPage = pagesCount - 3;
      endPage = pagesCount;
    } else {
      startPage = currentPage - 2;
      endPage = currentPage + 1;
    }
  }

  const pages = _.range(startPage, endPage + 1);

  return (
    <nav>
      <ul className="pagination flex-wrap">
        <li className={currentPage === 1 ? "disabled" : ""}>
          <button
            className="page-link"
            onClick={() => onPageChange(1)}
            disabled={currentPage === 1 ? true : false}
          >
            <i className="fa fa-angle-double-left" aria-hidden="true"></i>
          </button>
        </li>

        <li className={currentPage === 1 ? "disabled" : ""}>
          <button
            className="page-link"
            onClick={() => onPageChange(currentPage - 1)}
            disabled={currentPage === 1 ? true : false}
          >
            <i
              className="fa fa-angle-left"
              aria-hidden="true"
              style={{ width: "8px" }}
            ></i>
          </button>
        </li>

        {pages.map((page, index) => (
          <li
            key={index}
            className={page === currentPage ? "page-item active" : "page-item"}
          >
            <button className="page-link" onClick={() => onPageChange(page)}>
              {page}
            </button>
          </li>
        ))}

        <li className={currentPage === pagesCount ? "disabled" : ""}>
          <button
            className="page-link"
            onClick={() => onPageChange(currentPage + 1)}
            disabled={currentPage === pagesCount ? true : false}
          >
            <i
              className="fa fa-angle-right"
              aria-hidden="true"
              style={{ width: "8px" }}
            ></i>
          </button>
        </li>

        <li className={currentPage === pagesCount ? "disabled" : ""}>
          <button
            className="page-link"
            onClick={() => onPageChange(pagesCount)}
            disabled={currentPage === pagesCount ? true : false}
          >
            <i className="fa fa-angle-double-right" aria-hidden="true"></i>
          </button>
        </li>
      </ul>
    </nav>
  );
};

// Typechecking
Pagination.propTypes = {
  itemsCount: PropTypes.number.isRequired,
  pageSize: PropTypes.number.isRequired,
  onPageChange: PropTypes.func.isRequired,
  currentPage: PropTypes.number.isRequired,
};

export default Pagination;
