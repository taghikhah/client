// Libraries
import React, { Component } from "react";
import ReactTooltip from "react-tooltip";
import Lightbox from "react-image-lightbox";

const images = [""];

export default class LightboxExample extends Component {
  constructor(props) {
    super(props);

    this.state = {
      photoIndex: 0,
      isOpen: false,
    };
  }

  render() {
    const { photoIndex, isOpen } = this.state;

    return (
      <div>
        <button
          className="btn btn-sm btn-primary rounded"
          type="button"
          data-tip
          data-for="view"
          onClick={() => this.setState({ isOpen: true })}
        >
          <i className="fa fa-eye" aria-hidden="true"></i>
        </button>
        <ReactTooltip
          className="tooltip-size"
          id="view"
          place="top"
          effect="solid"
        >
          View
        </ReactTooltip>
        {isOpen && (
          <Lightbox
            mainSrc={this.props.image.url}
            nextSrc={""}
            prevSrc={""}
            onCloseRequest={() => this.setState({ isOpen: false })}
            onMovePrevRequest={() =>
              this.setState({
                photoIndex: (photoIndex + images.length - 1) % images.length,
              })
            }
            onMoveNextRequest={() =>
              this.setState({
                photoIndex: (photoIndex + 1) % images.length,
              })
            }
          />
        )}
      </div>
    );
  }
}
