// Axios
import http from "./httpService";

// Constants
const apiEnditemnt = "/item";
const tokenKey = process.env.REACT_APP_TOKEN_KEY;

// Get the current logged-in-user Token from the Browser
const jwt = localStorage.getItem(tokenKey);

// Define and get API routes
function itemUrl(id) {
  return `${apiEnditemnt}/${id}`;
}

// API request to get all of the Items
export function getItems() {
  return http.get(apiEnditemnt);
}

// API request to get the maximum Priority
export function getItemsMaxPriority(item) {
  if (item.poi_id) {
    return http.get(`${apiEnditemnt}/priority/${item.poi_id}`);
  }
}

// API request to get an Item by ID
export function getItem(item_id) {
  return http.get(itemUrl(item_id));
}

// API request to create a new Item or update an existing Item by ID
export function saveItem(item) {
  if (item._id) {
    const body = { ...item };
    delete body._id;
    return http.put(itemUrl(item._id), body);
  }

  return http.post(apiEnditemnt, item);
}

// API request to delete an Item by ID
export function deleteItem(item_id) {
  return http.delete(itemUrl(item_id), { headers: { "x-auth-token": jwt } });
}
