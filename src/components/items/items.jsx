// Libraries
import React, { Component } from "react";
import { Link } from "react-router-dom";
import { toast } from "react-toastify";
import _ from "lodash";

// Common Components
import Search from "../common/search";
import Filter from "../common/filter";
import TableMeta from "../common/table/tableMeta";
import Pagination from "../common/pagination";

// Child Components
import ItemsTable from "./itemsTable";
import Expire from "../common/expire";

// Functions
import { paginate } from "../../utils/scripts/paginate";
import { getItems, deleteItem } from "../../services/itemService";
import { getPois } from "../../services/poiService";
import { deleteFile } from "../../services/fileService";

class Items extends Component {
  state = {
    items: [],
    pois: [],
    currentPage: 1,
    initialSize: 3,
    pageSize: 3,
    searchQuery: "",
    selectedPoi: null,
    sortColumn: { path: "poi.title_en", order: "asc" },
  };

  // Retrive the Data from the Server
  async componentDidMount() {
    // Get Pois and Modify
    const { data: allPois } = await getPois();
    if (allPois.length === 0) return;

    // When at least a Poi exists in the database:
    const data = allPois.map(({ title_en: name, ...rest }) => ({
      name,
      ...rest,
    }));
    const pois = [{ _id: "", name: "All Histocaches" }, ...data];

    // Find related Poi
    function getPoi(id) {
      var poi = allPois.find((p) => {
        return p._id === id;
      });
      if (poi) return poi;
    }

    // Get Items and Modify
    const { data: allItems } = await getItems();

    if (allItems.length) {
      const items = allItems.map(({ poi_id: poi, ...rest }) => ({
        poi: {
          _id: getPoi(poi)._id,
          title_en: getPoi(poi).title_en,
          title_de: getPoi(poi).title_de,
        },
        ...rest,
      }));

      this.setState({ items, pois });
    }

    this.setState({ pois });
  }

  // Delete Event Handler
  handleDelete = async (item) => {
    const originalItems = this.state.items;
    const items = originalItems.filter((i) => i._id !== item._id);

    this.setState({ items });

    try {
      const response = await deleteItem(item._id);
      if (response.data && response.status === 200) {
        toast.success(
          <div>
            Item of priority: "{response.data.priority}"
            <br /> deleted successfully.
          </div>
        );

        await deleteFile(item.filename);
      }
    } catch (ex) {
      if (ex.response && ex.response.status === 401) {
        toast.error("Only Admins can delete an Item!");
      } else if (ex.response && ex.response.status === 404) {
        toast.error("This Item has already been deleted.");
      }

      this.setState({ items: originalItems });
    }
  };

  // Pagination Event Handler
  handlePageChange = (page) => {
    this.setState({ currentPage: page });
  };

  // PageSize Event Handler
  handleSizeChange = ({ currentTarget: input }) => {
    let pageSize = { ...this.state.pageSize };
    pageSize = input.value;

    this.setState({ pageSize });
  };

  // Filter Event Handler
  handlePoiSelect = ({ currentTarget: input }) => {
    const poi = this.state.pois.find((p) => p._id === input.value);

    this.setState({
      selectedPoi: poi,
      searchQuery: "",
      currentPage: 1,
    });
  };

  // Search Event Handler
  handleSearch = (query) => {
    this.setState({
      searchQuery: query,
      selectedPoi: null,
      currentPage: 1,
    });
  };

  // Sort Event Handler
  handleSort = (sortColumn) => {
    this.setState({ sortColumn });
  };

  // Get state Data
  getData = () => {
    const {
      currentPage,
      pageSize,
      items: allItems,
      selectedPoi,
      searchQuery,
      sortColumn,
    } = this.state;

    // Calculate StartNumber
    const startNumber = (currentPage - 1) * pageSize;

    // Get Filter Options
    let filtered = allItems;

    if (searchQuery)
      filtered = allItems.filter(
        (i) =>
          i.poi.title_en.toLowerCase().indexOf(searchQuery.toLowerCase()) !==
            -1 ||
          i.poi.title_de.toLowerCase().indexOf(searchQuery.toLowerCase()) !== -1
      );
    else if (selectedPoi && selectedPoi._id)
      filtered = allItems.filter((i) => i.poi._id === selectedPoi._id);

    // Define Sorting Option
    const sorted = _.orderBy(filtered, [sortColumn.path], [sortColumn.order]);

    // Define Pagination
    const items = paginate(sorted, currentPage, pageSize);

    return {
      startNumber: startNumber,
      totalCount: filtered.length,
      data: items,
    };
  };

  render() {
    const { length: count } = this.state.items;
    const {
      currentPage,
      pageSize,
      sortColumn,
      searchQuery,
      pois,
      initialSize,
    } = this.state;
    const { startNumber, totalCount, data: items } = this.getData();

    return (
      <div className="col alt-table-responsive mt-1">
        <div className="row justify-content-between align-items-center underline my-2">
          <div className="col-md-6 mt-2">
            <h2>
              <i className="bx bx-windows"></i> Items
            </h2>
          </div>
          <div className="col-sm-12 col-md-5 col-lg-4 col-xl-3 mb-2 text-end">
            {pois.length === 0 && (
              <Link
                className="btn btn-outline-dark btn-block rounded"
                to="/dashboard/pois/"
              >
                <i className="fa fa-exclamation-circle" aria-hidden="true"></i>{" "}
                First create a new Histocache{" "}
              </Link>
            )}
            {pois.length !== 0 && (
              <Link
                className="btn btn-warning btn-block rounded"
                to="/dashboard/item/new"
              >
                <i className="fa fa-plus-circle" aria-hidden="true"></i> Create
                a new Item
              </Link>
            )}
          </div>
        </div>

        {count !== 0 && (
          <React.Fragment>
            <div className="row justify-content-between align-items-center pb-1">
              <div className="col-md-6 my-2">
                <Filter
                  name={"dropdown"}
                  label={"Filter"}
                  options={this.state.pois}
                  selectedOption={this.state.selectedPoi}
                  onChange={this.handlePoiSelect}
                />
              </div>
              <div className="col-sm-12 col-md-5 col-lg-4 col-xl-3 my-2 text-end">
                <Search value={searchQuery} onChange={this.handleSearch} />
              </div>
            </div>

            <div className="row outer mt-1">
              <div className="col">
                <ItemsTable
                  items={items}
                  sortColumn={sortColumn}
                  startNumber={startNumber}
                  onDelete={this.handleDelete}
                  onSort={this.handleSort}
                />
              </div>
            </div>
          </React.Fragment>
        )}

        {count === 0 && (
          <div className="row">
            <div className="col-sm-12 align-self-center">
              <Expire delay={5000}>
                <div className="loader">
                  <div></div>
                  <div></div>
                  <div></div>
                </div>
              </Expire>
            </div>
          </div>
        )}

        <hr />

        <div className="row align-items-center">
          <div className="col-sm-12 col-md col-lg col-xl mr-auto pb-1">
            <Pagination
              itemsCount={totalCount}
              pageSize={pageSize}
              currentPage={currentPage}
              onPageChange={this.handlePageChange}
            />
          </div>
          <div className="col-auto align-middle">
            <TableMeta
              initial={initialSize}
              itemsCount={totalCount}
              total={count}
              onChange={this.handleSizeChange}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default Items;
