// Libraries
import React from "react";
import PropTypes from "prop-types";

const Filter = ({
  name,
  label,
  options,
  selectedOption,
  onOptionSelect,
  ...rest
}) => {
  return (
    <div className="input-group">
      <div className="input-group-prepend">
        <label className="input-group-text left-rounded" htmlFor={name}>
          {label}
        </label>
      </div>

      <select
        className="custom-select right-rounded"
        id={name}
        name={name}
        {...rest}
      >
        {options.map((option) => (
          <option key={option._id} value={option._id}>
            {option.name}
          </option>
        ))}
      </select>
    </div>
  );
};

// Typechecking
Filter.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  options: PropTypes.array.isRequired,
  onOptionSelect: PropTypes.func,
};

export default Filter;
