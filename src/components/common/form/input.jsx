// Libraries
import React from "react";
import PropTypes from "prop-types";

const Input = ({ name, label, error, value, text, status, ...rest }) => {
  return (
    <div className="form-group">
      <label htmlFor={name}>{label}</label>
      <input
        {...rest}
        id={name}
        name={name}
        value={value}
        className="form-control rounded"
        disabled={false ^ status}
      />
      <small className="w-100" style={{ color: "red" }}>
        {text}
      </small>
      {error && <div className="alert alert-danger">{error}</div>}
    </div>
  );
};

// Typechecking
Input.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  status: PropTypes.bool,
};

export default Input;
