// Libraries
import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

const Header = ({ user }) => {
  return (
    <header className="header" id="header">
      <div className="header_toggle">
        {" "}
        <i className="bx bx-menu" id="header-toggle"></i>{" "}
      </div>

      <div className="header_user">
        <div className="header_img">
          <img src={`${process.env.PUBLIC_URL}/profile.png`} alt="profile" />{" "}
        </div>
        <Link
          className="nav-link dropdown-toggle"
          to="#"
          id="profile"
          data-toggle="dropdown"
          aria-haspopup="true"
          aria-expanded="false"
          style={{ color: "#fec000" }}
        >
          {user.name}
        </Link>
        <div
          className="dropdown-menu dropdown-menu-right"
          aria-labelledby="profile"
        >
          <Link className="dropdown-item" to="/dashboard/user/profile">
            Profile
          </Link>
          <Link className="dropdown-item" to={"/dashboard/user/" + user._id}>
            Setting
          </Link>
          <div className="dropdown-divider"></div>
          <Link className="dropdown-item" to="/logout">
            <i
              className="bx bx-log-out bx-xs"
              style={{ verticalAlign: "text-top" }}
            ></i>{" "}
            Logout
          </Link>
        </div>
      </div>
    </header>
  );
};

// Typechecking
Header.propTypes = {
  user: PropTypes.object.isRequired,
};

export default Header;
