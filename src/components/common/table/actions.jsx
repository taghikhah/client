import React from "react";
import { Link } from "react-router-dom";
import ReactTooltip from "react-tooltip";
import InlineConfirmButton from "react-inline-confirm";

const Actions = (props) => {
  const textValues = ["", " Sure?", " Deleting..."];
  const confirmIconClass = "fa fa-trash";
  /*
  const confirmIconClass = `fa fa-${
    props.isExecuting ? "circle-o-notch fa-spin" : "trash" 
  }`;
*/
  return (
    <div className="row">
      <div className="col-sm-3 p-1 mr-3">
        <Link
          className="btn btn-sm btn-primary btn-circle rounded"
          to={`/dashboard/${props.path}/${props.item._id}`}
          data-tip
          data-for="edit"
        >
          <i className="fa fa-pencil" aria-hidden="true"></i>
        </Link>
        <ReactTooltip
          className="tooltip-size"
          id="edit"
          place="top"
          effect="solid"
        >
          Edit
        </ReactTooltip>
      </div>
      <div className="p-1" style={{ flex: "0px !important" }}>
        <InlineConfirmButton
          className="btn btn-sm btn-danger btn-circle rounded"
          type="button"
          data-tip
          data-for="delete"
          textValues={textValues}
          showTimer
          isExecuting={false}
          onClick={props.onClick}
        >
          <i
            className={confirmIconClass}
            style={{
              paddingRight: "0.05rem",
              paddingLeft: "0.05rem",
            }}
          ></i>
        </InlineConfirmButton>
        <ReactTooltip
          className="tooltip-size"
          id="delete"
          place="top"
          effect="solid"
        >
          Delete
        </ReactTooltip>
      </div>
    </div>
  );
};

export default Actions;

/*
       
        <button
          onClick={props.onClick}
          className="btn btn-danger btn-circle btn-sm rounded"
          type="button"
          data-tip
          data-for="delete"
        >
          <i
            className="fa fa-trash"
            aria-hidden="true"
            style={{ verticalAlign: "text-top" }}
          ></i>{" "}
          Delete
        </button>



*/

/*
      <button onClick={() => this.props.onLike(movie)} className="btn btn-sm btn-primary mr-2">
        <i className="fa fa-pencil" aria-hidden="true"></i>
      </button>

      <div className="col-lg-4 p-1">
        <Link
          className="btn btn-sm btn-primary mr-2 btn-circle btn-sm"
          to={`/pois/${props.view}`}
          style={{
            borderRadius: "50px",
          }}
          data-tip
          data-for="web-view"
        >
          <i className="fa fa-eye" aria-hidden="true"></i>
        </Link>
        <ReactTooltip id="web-view" place="top" effect="solid">
          Web View
        </ReactTooltip>
      </div>

*/
