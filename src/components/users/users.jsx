// Libraries
import React, { Component } from "react";
import { Link } from "react-router-dom";
import { toast } from "react-toastify";
import _ from "lodash";

// Child Components
import UsersTable from "./usersTable";
import Expire from "../common/expire";
import Pagination from "../common/pagination";
import TableMeta from "../common/table/tableMeta";
import Search from "../common/search";

// Functions
import { getUsers, deleteUser } from "../../services/userService";
import { paginate } from "../../utils/scripts/paginate";

class Users extends Component {
  state = {
    users: [],
    currentPage: 1,
    initialSize: 5,
    pageSize: 5,
    searchQuery: "",
    sortColumn: { path: "name", order: "asc" },
  };

  // Retrive data from the server
  async componentDidMount() {
    const { data: allUsers } = await getUsers();
    const users = allUsers.filter((u) => u.role !== "owner");

    this.setState({ users });
  }

  // Delete event handler
  handleDelete = async (user) => {
    const originalUsers = this.state.users;
    const users = originalUsers.filter((u) => u._id !== user._id);

    this.setState({ users });

    try {
      const response = await deleteUser(user._id);
      if (response.data && response.status === 200) {
        toast.success(`The User ${response.data.name} deleted successfully.`);
      }
    } catch (ex) {
      if (ex.response && ex.response.status === 404) {
        toast.error("This user has already been deleted.");
      }

      this.setState({ users: originalUsers });
    }
  };

  // Pagination event handler
  handlePageChange = (page) => {
    this.setState({ currentPage: page });
  };

  // Page size event handler
  handleSizeChange = ({ currentTarget: input }) => {
    let pageSize = { ...this.state.pageSize };
    pageSize = input.value;

    this.setState({ pageSize });
  };

  // Search event handler
  handleSearch = (query) => {
    this.setState({
      searchQuery: query,
      selectedUser: null,
      currentPage: 1,
    });
  };

  // Sort event handler
  handleSort = (sortColumn) => {
    this.setState({ sortColumn });
  };

  getData = () => {
    const {
      currentPage,
      pageSize,
      users: allUsers,
      sortColumn,
      searchQuery,
    } = this.state;

    // Start number
    const startNumber = (currentPage - 1) * pageSize;

    // Filter option
    let filtered = allUsers;
    if (searchQuery)
      filtered = allUsers.filter(
        (u) => u.name.toLowerCase().indexOf(searchQuery.toLowerCase()) !== -1
      );

    // Sorting option
    const sorted = _.orderBy(filtered, [sortColumn.path], [sortColumn.order]);

    // Pagination option
    const users = paginate(sorted, currentPage, pageSize);

    return {
      startNumber: startNumber,
      totalCount: filtered.length,
      data: users,
    };
  };

  render() {
    const { length: count } = this.state.users;
    const { currentPage, pageSize, sortColumn, searchQuery, initialSize } =
      this.state;
    const { startNumber, totalCount, data: users } = this.getData();
    const { user } = this.props;

    return (
      <div className="col alt-table-responsive mt-1">
        <div className="row justify-content-between align-items-center underline my-2">
          <div className="col-md-6 mt-2">
            <h2>
              <i className="bx bxs-group"></i> Users
            </h2>
          </div>
          <div className="col-sm-12 col-md-5 col-lg-4 col-xl-3 mb-2 text-end ">
            {user === "owner" && (
              <Link
                className="btn btn-warning btn-block rounded"
                to="/dashboard/user/new"
              >
                <i className="fa fa-plus-circle" aria-hidden="true"></i> Create
                a new User
              </Link>
            )}
          </div>
        </div>

        <div className="row justify-content-between align-items-center pb-1">
          <div className="col-md-6 my-2"></div>
          <div className="col-sm-12 col-md-5 col-lg-4 col-xl-3 my-2 text-end ">
            <Search value={searchQuery} onChange={this.handleSearch} />
          </div>
        </div>

        {count !== 0 && (
          <div className="row outer mt-1">
            <div className="col">
              <UsersTable
                users={users}
                sortColumn={sortColumn}
                startNumber={startNumber}
                onDelete={this.handleDelete}
                onSort={this.handleSort}
              />
            </div>
          </div>
        )}

        {count === 0 && (
          <div className="row">
            <div className="col-sm-12 align-self-center">
              <Expire delay={5000}>
                <div className="loader">
                  <div></div>
                  <div></div>
                  <div></div>
                </div>
              </Expire>
            </div>
          </div>
        )}

        <hr />

        <div className="row align-items-center">
          <div className="col-sm-12 col-md col-lg col-xl mr-auto pb-1">
            <Pagination
              itemsCount={totalCount}
              pageSize={pageSize}
              currentPage={currentPage}
              onPageChange={this.handlePageChange}
            />
          </div>
          <div className="col-auto align-middle">
            <TableMeta
              initial={initialSize}
              itemsCount={totalCount}
              total={count}
              onChange={this.handleSizeChange}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default Users;
