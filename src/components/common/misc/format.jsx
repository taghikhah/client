// Libraries
import React from "react";

const reformatDate = (e) => {
  const date = new Date(e);
  const options = {
    year: "numeric",
    month: "short",
    day: "numeric",
    hour: "2-digit",
    minute: "2-digit",
    hour12: false,
  };

  return new Intl.DateTimeFormat("en-GB", options).format(date);
};

const Format = ({ date }) => {
  return <p>{reformatDate(date)}</p>;
};

export default Format;
