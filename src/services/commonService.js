// Libraries
import axios from "axios";

// Constants
const baseURL = process.env.REACT_APP_API_URL;

export default axios.create({
  baseURL,
  headers: {
    "Content-type": "application/json",
  },
});
