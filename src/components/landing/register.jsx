// Libraries
import React from "react";
import { Link } from "react-router-dom";
import Joi from "joi-browser";

// Child Components
import Form from "../common/form";

// Functions
import userService from "../../services/userService";
import authService from "../../services/authService";

class Register extends Form {
  state = {
    data: { name: "", email: "", password: "" },
    errors: { name: "", email: "", password: "" },
  };

  schema = {
    name: Joi.string().required().label("Name"),
    email: Joi.string().email().required().label("Email"),
    password: Joi.string().min(8).required().label("Password"),
  };

  doSubmit = async () => {
    try {
      const response = await userService.registerUsers(this.state.data);

      authService.loginWithJwt(response.headers["x-auth-token"]);

      window.location = "/dashboard";
    } catch (ex) {
      if (ex.response && ex.response.status === 400) {
        const errors = { ...this.state.errors };
        errors.email = ex.response.data;

        this.setState({ errors });
      }
    }
  };

  render() {
    return (
      <div className="col alt-table-responsive">
        <div className="row align-items-center d-flex justify-content-center">
          <div className="col-sm-9 col-md-5 col-lg-5 col-xl-4 mt-4">
            <div className="card card-signin my-5">
              <div className="card-body">
                <h5 className="card-title text-center">Register</h5>

                <form className="form-signin" onSubmit={this.handleSubmit}>
                  {this.renderInput("name", "Name")}
                  {this.renderInput("email", "Email")}
                  {this.renderInput(
                    "password",
                    "Password",
                    "password",
                    "* a complex password is required!"
                  )}
                  <div className="mt-4 text-center">
                    {this.renderButton(
                      "Register",
                      "btn btn-lg btn-warning btn-block text-uppercase",
                      ""
                    )}
                  </div>

                  <hr className="my-4" />

                  <div className="form-label-group text-center">
                    <span htmlFor="inputEmail">Already have an account? </span>
                    <Link to="/login"> Login Here</Link>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Register;
