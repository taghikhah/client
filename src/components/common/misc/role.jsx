// Libraries
import React, { Component } from "react";
import PropTypes from "prop-types";

class Role extends Component {
  getBadgeClasses() {
    let classes = "badge badge-pill badge-";

    const role = this.props.role;
    switch (role) {
      case "owner":
        classes += "success";
        break;
      case "admin":
        classes += "primary";
        break;
      default:
        classes += "secondary";
    }

    return classes;
  }

  capitalize(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

  render() {
    return (
      <span className={this.getBadgeClasses()}>
        {this.capitalize(this.props.role)}
      </span>
    );
  }
}

// Typechecking
Role.propTypes = {
  role: PropTypes.string.isRequired,
};

export default Role;
