// Libraries
import React from "react";
import Joi from "joi-browser";

// Child Components
import Form from "../common/form";
import Back from "../common/form/back";

// Functions
import userService from "../../services/userService";
import authService from "../../services/authService";

class UserForm extends Form {
  state = {
    data: { name: "", email: "", password: "" },
    errors: { name: "", email: "", password: "" },
    currentUserRole: "",
    roles: [
      { name: "owner", _id: "owner" },
      { name: "admin", _id: "admin" },
      { name: "editor", _id: "editor" },
    ],
  };

  schema = {
    _id: Joi.string(),
    role: Joi.string().label("Role"),
    name: Joi.string().label("Name"),
    email: Joi.string().email().label("Email"),
    password: Joi.string().min(8).label("Password"),
  };

  async componentDidMount() {
    const currentUser = authService.getCurrentUser();
    const currentUserRole = currentUser.role;
    this.setState({ currentUserRole });

    // Get Poi related to the Given ID
    const user_id = this.props.match.params.id;

    if (user_id === "new") return;

    const { data: user } = await userService.findUser(user_id);
    if (!user) return this.props.history.replace("/not-found");

    this.setState({ data: this.mapToViewModel(user) });
  }

  mapToViewModel(user) {
    return {
      _id: user._id,
      name: user.name,
      email: user.email,
      password: user.password,
      role: user.role,
    };
  }

  doSubmit = async () => {
    const id = this.props.match.params.id;
    const { currentUserRole } = this.state;

    // Owner functionality
    if (currentUserRole === "owner") {
      try {
        if (id !== "new") {
          await userService.upgradeUser(this.state.data);
        }
        await userService.saveUser(this.state.data);

        // routing after submitting the form
        this.props.history.push("/dashboard/users");
      } catch (ex) {
        if (ex.response && ex.response.status === 400) {
          const errors = { ...this.state.errors };
          errors.email = ex.response.data;
          this.setState({ errors });
        }
      }
    } else {
      // non-Owner functionality
      try {
        await userService.saveUser(this.state.data);

        // routing after submitting the form
        window.location = "/logout";
      } catch (ex) {
        if (ex.response && ex.response.status === 400) {
          const errors = { ...this.state.errors };
          errors.email = ex.response.data;
          this.setState({ errors });
        }
      }
    }
  };

  handleBack = () => {
    this.props.history.push("/dashboard/users");
  };

  getStatus = () => {
    const currentUser = authService.getCurrentUser();
    const id = this.props.match.params.id;

    if (currentUser.role === "editor") {
      if (currentUser._id === id) {
        return false;
      }
      return true;
    }

    return false;
  };

  render() {
    const id = this.props.match.params.id;
    const { roles, currentUserRole } = this.state;
    const status = this.getStatus();

    return (
      <div className="col alt-table-responsive mt-1 mb-1">
        <div className="row align-items-center mt-1 mb-2 underline">
          <div className="col mt-2">
            <h2 className="mt-2">
              <i className="bx bxs-user"></i>{" "}
              {id === "new" ? "New User" : "Edit User Profile"}
            </h2>
          </div>
        </div>

        <form onSubmit={this.handleSubmit}>
          <div className="row align-items-center pt-2 my-2">
            <div className="col-sm-6">
              {this.renderInput("name", "Name", "text", "", status)}
              {this.renderInput("email", "Email", "text", "", status)}
              {this.renderPassword(
                "password",
                "Password",
                "* new password must be set each time before saving!",
                status
              )}
              {this.renderSelect(
                "role",
                "Role",
                roles,
                currentUserRole === "owner" ? false : true
              )}
            </div>
            <div className="col-sm-6">
              <div className="container">
                <div className="content">
                  <div className="card rounded-semi" style={{ width: "100%" }}>
                    <div className="card-body">
                      <img
                        src={`${process.env.PUBLIC_URL}/profile.png`}
                        alt="profile"
                        style={{ width: "100%" }}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <hr />

          <div className="row justify-content-between align-items-center mt-3 pb-2">
            <div className="col">
              <Back onClick={this.handleBack} />
            </div>

            <div className="col text-end">
              {this.renderButton("Save", "btn btn-outline-success rounded")}
            </div>
          </div>
        </form>
      </div>
    );
  }
}

export default UserForm;
