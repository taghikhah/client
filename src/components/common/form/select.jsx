// Libraries
import React from "react";
import PropTypes from "prop-types";

const Select = ({ name, label, options, status, error, ...rest }) => {
  return (
    <div className="form-group">
      <label className="my-1 mr-2" htmlFor={name}>
        {label}
      </label>
      <select
        className="custom-select my-1 mr-sm-2 rounded"
        id={name}
        name={name}
        {...rest}
        disabled={false ^ status}
      >
        <option defaultValue="">Choose...</option>
        {options.map((option) => (
          <option key={option._id} value={option._id}>
            {option.name}
          </option>
        ))}
      </select>
      {error && <div className="alert alert-danger">{error}</div>}
    </div>
  );
};

// Typechecking
Select.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  options: PropTypes.array.isRequired,
  status: PropTypes.bool,
};

export default Select;
