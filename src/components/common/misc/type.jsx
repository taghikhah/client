// Libraries
import React, { Component } from "react";
import PropTypes from "prop-types";

class Type extends Component {
  getBadgeClasses() {
    let classes = "badge badge-pill badge-";

    const type = this.props.type;
    switch (type) {
      case "Histocache":
        classes += "success";
        break;
      case "Viewpoint":
        classes += "secondary";
        break;
      default:
        classes += "warning";
    }

    return classes;
  }

  render() {
    return <span className={this.getBadgeClasses()}>{this.props.type}</span>;
  }
}

// Typechecking
Type.propTypes = {
  type: PropTypes.string.isRequired,
};

export default Type;
