// Libraries
import React from "react";
import { Switch, Redirect } from "react-router-dom";

// Common Components
import Private from "../../routes/private";

// Child Components
import Header from "./header";
import SideBar from "./sideBar";
import Overview from "../overview/overview";
import Categories from "../categories/categories";
import CategoryForm from "../categories/categoryForm";
import Pois from "../pois/pois";
import PoiForm from "../pois/poiForm";
import Items from "../items/items";
import ItemForm from "../items/itemForm";
import Users from "../users/users";
import UserForm from "../users/userForm";
import Profile from "../users/profile";

class Dashboard extends React.Component {
  constructor() {
    super();
    this.handleClick = this.handleClick.bind(this);
  }

  componentDidMount() {
    this.handleClick();
  }

  // SideBar toggle button Evenet Handler
  handleClick() {
    const renderSidebar = (toggleId, navId, bodyId, headerId) => {
      const toggle = document.getElementById(toggleId),
        nav = document.getElementById(navId),
        bodypd = document.getElementById(bodyId),
        headerpd = document.getElementById(headerId);

      // Validate that all variables exist
      if (toggle && nav && bodypd && headerpd) {
        toggle.addEventListener("click", () => {
          // Show SideBar
          nav.classList.toggle("show");
          // Change toggle Icon
          toggle.classList.toggle("bx-x");
          // Add padding to Body
          bodypd.classList.toggle("body-pd");
          // Add padding to Header
          headerpd.classList.toggle("header-pd");
        });
      }
    };

    // Render SiderBar Function
    renderSidebar("header-toggle", "nav-bar", "body-pd", "header");

    // Link color
    const linkColor = document.querySelectorAll(".nav_link");

    // Active link event handler
    function activateLink() {
      if (linkColor) {
        linkColor.forEach((l) => l.classList.remove("active"));
        this.classList.add("active");
      }
    }

    linkColor.forEach((l) => l.addEventListener("click", activateLink));
  }

  render() {
    const { user } = this.props;

    return (
      <div className="body">
        <div id="body-pd">
          <Header user={user} />
          <SideBar role={user.role} />
          <div className="bg-light contain-bg">
            <Switch>
              <Private exact path="/dashboard/overview" component={Overview} />
              <Private
                path="/dashboard/category/:id"
                component={CategoryForm}
              />
              <Private path="/dashboard/categories" component={Categories} />
              <Private path="/dashboard/poi/:id" component={PoiForm} />
              <Private path="/dashboard/pois" component={Pois} />
              <Private path="/dashboard/item/:id" component={ItemForm} />
              <Private path="/dashboard/items" component={Items} />
              <Private
                path="/dashboard/user/profile"
                render={(props) => <Profile {...props} user={user} />}
              />
              <Private exact path="/dashboard/user/:id" component={UserForm} />
              <Private
                path="/dashboard/users"
                render={(props) => <Users {...props} user={user.role} />}
              />
              <Redirect from="/dashboard" exact to="/dashboard/overview" />
              <Redirect from="/logout" exact to="/" />
              <Redirect to="/not-found" />
            </Switch>
          </div>
        </div>
      </div>
    );
  }
}

export default Dashboard;
