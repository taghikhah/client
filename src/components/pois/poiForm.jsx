// Libraries
import React from "react";
import Joi from "joi-browser";
import { toast } from "react-toastify";

// Parent Component
import Form from "../common/form";
import Back from "../common/form/back";

// Functions
import { getCategories } from "../../services/categoryService";
import { uploadFiles } from "../../services/fileService";
import { getPoi, savePoi, getPoisMaxPriority } from "../../services/poiService";

class PoiFrom extends Form {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleFileSelect = this.handleFileSelect.bind(this);
    this.handleImageLoad = this.handleImageLoad.bind(this);
    this.handleLabel = this.handleLabel.bind(this);
    this.handleMapClick = this.handleMapClick.bind(this);

    this.state = {
      data: {
        category_id: "",
        priority: "",
        title_en: "",
        title_de: "",
        description_en: "",
        description_de: "",
        caption_en: "",
        caption_de: "",
        add_info_url: "",
        is_displayed_on_table: false,
        has_histocache_location: false,
        lat: "",
        long: "",
        filename: "",
        image_aspect_ratio: "",
        has_viewpoint_location: false,
        viewpoint_lat: "",
        viewpoint_long: "",
        viewpoint_filename: "",
        viewpoint_image_aspect_ratio: "",
        viewpoint_image_height: "",
        viewpoint_image_offset: "",
        viewpoint_image_vertical_offset: "",
      },
      maxPriority: "",
      categories: [],
      selectedFiles: { filename: "", viewpoint_filename: "" },
      previewImages: [],
      selectedImages: [],
      images: [],
      inputLabels: { filename: "", viewpoint_filename: "" },
      labels: [],
      dimensions: {},
      errors: {},
    };
  }

  // Define the Form Schema
  schema = {
    _id: Joi.string(),
    category_id: Joi.string().required().label("Category"),
    priority: Joi.number().integer().min(1).max(100).label("Priority"),
    title_en: Joi.string().min(5).max(100).required().label("Title (English)"),
    title_de: Joi.string().min(5).max(100).required().label("Title (German)"),
    description_en: Joi.string()
      .min(50)
      .max(5000)
      .required()
      .label("Description (English)"),
    description_de: Joi.string()
      .min(50)
      .max(5000)
      .required()
      .label("Description (German)"),
    caption_en: Joi.string()
      .min(5)
      .max(500)
      .required()
      .label("Caption (English)"),
    caption_de: Joi.string()
      .min(5)
      .max(500)
      .required()
      .label("Caption (German)"),
    add_info_url: Joi.string()
      .optional()
      .allow(null)
      .allow("")
      .empty("")
      .label("Add Info URL"),

    is_displayed_on_table: Joi.boolean().label("Displayed on Table").required(),

    has_histocache_location: Joi.boolean()
      .label("Histocache Location")
      .required(),
    lat: Joi.number()
      .min(51.0)
      .max(51.1)
      .label("Latitude")
      .when("has_histocache_location", {
        is: true,
        then: Joi.required(),
        otherwise: Joi.allow(null).allow("").empty(""),
      }),
    long: Joi.number()
      .min(13.0)
      .max(13.9)
      .label("Longitude")
      .when("has_histocache_location", {
        is: true,
        then: Joi.required(),
        otherwise: Joi.allow(null).allow("").empty(""),
      }),
    filename: Joi.string().label("File Name").required(),

    image_aspect_ratio: Joi.number()
      .min(0.1)
      .max(4)
      .label("Aspect Ratio")
      .when("has_histocache_location", {
        is: true,
        then: Joi.required(),
        otherwise: Joi.allow(null).allow("").empty(""),
      }),

    has_viewpoint_location: Joi.boolean()
      .label("Viewpoint Location")
      .required(),
    viewpoint_lat: Joi.number()
      .min(51.0)
      .max(51.1)
      .label("Latitude")
      .when("has_viewpoint_location", {
        is: true,
        then: Joi.required(),
        otherwise: Joi.allow(null).allow("").empty(""),
      }),
    viewpoint_long: Joi.number()
      .min(13.0)
      .max(13.9)
      .label("Longitude")
      .when("has_viewpoint_location", {
        is: true,
        then: Joi.required(),
        otherwise: Joi.allow(null).allow("").empty(""),
      }),
    viewpoint_filename: Joi.string()
      .label("File Name")
      .when("has_viewpoint_location", {
        is: true,
        then: Joi.required(),
        otherwise: Joi.allow(null).allow("").empty(""),
      }),
    viewpoint_image_aspect_ratio: Joi.number()
      .min(0.1)
      .max(4)
      .label("Aspect Ratio")
      .when("has_viewpoint_location", {
        is: true,
        then: Joi.required(),
        otherwise: Joi.allow(null).allow("").empty(""),
      }),
    viewpoint_image_height: Joi.number()
      .min(1)
      .label("Image Height")
      .when("has_viewpoint_location", {
        is: true,
        then: Joi.required(),
        otherwise: Joi.allow(null).allow("").empty(""),
      }),
    viewpoint_image_offset: Joi.number()
      .min(1)
      .label("Image Offset")
      .when("has_viewpoint_location", {
        is: true,
        then: Joi.required(),
        otherwise: Joi.allow(null).allow("").empty(""),
      }),
    viewpoint_image_vertical_offset: Joi.number()
      .label("Vertical Offset")
      .when("has_viewpoint_location", {
        is: true,
        then: Joi.required(),
        otherwise: Joi.allow(null).allow("").empty("").allow(0),
      }),
  };

  // Retrive the Data from the Server
  async componentDidMount() {
    // Get Categories and Modify
    const { data: allCategories } = await getCategories();
    if (allCategories.length === 0) return;

    // When at least a Poi exists in the database:
    const categories = allCategories.map(({ name_en: name, ...rest }) => ({
      name,
      ...rest,
    }));

    this.setState({ categories });

    // Get the maximum Priority
    const { data: max } = await getPoisMaxPriority();

    // Get the Given ID
    const poi_id = this.props.match.params.id;

    if (poi_id === "new") {
      const data = { ...this.state.data };

      if (max.length === 0) {
        data.priority = 1;
        return this.setState({ data, maxPriority: 1 });
      } else {
        data.priority = max[0].priority + 1;
        return this.setState({ data, maxPriority: max[0].priority });
      }
    }

    // Get Poi related to the Given ID
    const { data: poi } = await getPoi(poi_id);
    if (!poi) return this.props.history.replace("/not-found");

    this.setState({
      data: this.mapToViewModel(poi),
      maxPriority: max[0].priority,
    });
  }

  // Map current Poi to the Form Inputs
  mapToViewModel(poi) {
    return {
      _id: poi._id,
      priority: poi.priority,
      category_id: poi.category_id,
      title_en: poi.title_en,
      title_de: poi.title_de,
      description_en: poi.description_en,
      description_de: poi.description_de,
      caption_en: poi.caption_en,
      caption_de: poi.caption_de,
      add_info_url: poi.add_info_url,
      is_displayed_on_table: poi.is_displayed_on_table,
      has_histocache_location: poi.has_histocache_location,
      lat: poi.lat,
      long: poi.long,
      filename: poi.filename,
      image_aspect_ratio: poi.image_aspect_ratio,
      has_viewpoint_location: poi.has_viewpoint_location,
      viewpoint_lat: poi.viewpoint_lat,
      viewpoint_long: poi.viewpoint_long,
      viewpoint_filename: poi.viewpoint_filename,
      viewpoint_image_aspect_ratio: poi.viewpoint_image_aspect_ratio,
      viewpoint_image_height: poi.viewpoint_image_height,
      viewpoint_image_offset: poi.viewpoint_image_offset,
      viewpoint_image_vertical_offset: poi.viewpoint_image_vertical_offset,
    };
  }

  // Form submit Function
  doSubmit = async () => {
    // Poi to Save
    const data = { ...this.state.data };
    const cleanPoi = this.cleanDataObject(data);

    // Save Item to the Database
    try {
      const response = await savePoi(cleanPoi);
      if (response.data && response.status === 200) {
        toast.success(
          <div>
            Histocache: "{response.data.title_en}" <br /> saved successfully.
          </div>
        );

        // Images to Save
        const selectedImages = [...this.state.selectedImages];
        // Image Upload Header
        const header = response.data._id;

        for (let i = 0; i < selectedImages.length; i++) {
          await uploadFiles(selectedImages[i], header, () => {});
        }
      }
    } catch (ex) {
      if (ex.response && ex.response.status === 400) {
        toast.error("This Histocache has already exists!");
      }
      return;
    }

    // Return to Pois TableView after saving
    this.props.history.push("/dashboard/pois");
  };

  // Data Object cleaning Function
  cleanDataObject(obj) {
    for (var property in obj) {
      if (
        obj[property] === null ||
        obj[property] === undefined ||
        obj[property] === ""
      ) {
        delete obj[property];
      }
    }
    return obj;
  }

  // Back button Event Handler
  handleBack = () => {
    this.props.history.push("/dashboard/pois");
  };

  render() {
    const id = this.props.match.params.id;

    return (
      <div className="col alt-table-responsive mt-1 mb-1">
        <div className="row align-items-center mt-1 mb-2 underline">
          <div className="col mt-2">
            <h2 className="mt-2">
              <i className="fa fa-map-pin" aria-hidden="true"></i>{" "}
              {id === "new" ? "New Histocache" : "Edit Histocache Details"}
            </h2>
          </div>
        </div>
        <form
          onSubmit={this.handleSubmit}
          className="form-group"
          encType="multipart/form-data"
        >
          <div className="row align-items-center pt-2 my-2">
            <div className="col-sm-6">
              {this.renderSelect(
                "category_id",
                "Category",
                this.state.categories
              )}
              {this.renderPriority("priority", "Priority")}
              {this.renderFileInput("filename")}
              <div className="row mt-3">
                <div className="col-sm-4">
                  {this.renderInput(
                    "image_aspect_ratio",
                    "Aspect Ratio",
                    "number",
                    "",
                    true
                  )}
                </div>
                <div className="col-sm-8">
                  {this.renderInput("add_info_url", "Additional info URL")}
                </div>
              </div>
            </div>
            <div className="col-sm-6">
              {this.renderPreview("image_aspect_ratio", "filename")}
            </div>
          </div>

          <hr />

          <div className="row pt-2">
            <div className="col-sm-6">
              {this.renderInput("title_en", "Title (English)")}
            </div>

            <div className="col-sm-6">
              {this.renderInput("title_de", "Title (German)")}
            </div>
          </div>

          <div className="row">
            <div className="col-sm-6">
              {this.renderTextBox("description_en", "Description (English)")}
            </div>

            <div className="col-sm-6">
              {this.renderTextBox("description_de", "Description (German)")}
            </div>
          </div>

          <div className="row">
            <div className="col-sm-6">
              {this.renderTextBox("caption_en", "Caption (English)")}
            </div>
            <div className="col-sm-6">
              {this.renderTextBox("caption_de", "Caption (German)")}
            </div>
          </div>

          <hr />

          <div className="row mb-3">
            <div className="col">
              {this.renderToggle("is_displayed_on_table", "Displayed on Table")}
            </div>
          </div>

          <hr />

          <div className="row mb-3">
            <div className="col">
              {this.renderToggle(
                "has_histocache_location",
                "Histocache Location"
              )}
            </div>
          </div>

          {this.state.data.has_histocache_location && (
            <div className="row">
              <div className="col-sm-6">
                {this.renderMap("lat", "long")}
                <div className="row pt-3">
                  <div className="col-sm-6">
                    {this.renderInput("lat", "Latitude", "number")}
                  </div>
                  <div className="col-sm-6">
                    {this.renderInput("long", "Longitude", "number")}
                  </div>
                </div>
              </div>

              <div className="col-sm-6">
                <div className="row">
                  <div className="col"></div>
                </div>
                <div className="row mt-3">
                  <div className="col-sm-4"></div>
                </div>
                <div className="row">
                  <div className="col-sm-12"></div>
                </div>
              </div>
            </div>
          )}

          <hr />

          <div className="row mb-3">
            <div className="col">
              {this.renderToggle(
                "has_viewpoint_location",
                "Viewpoint Location"
              )}
            </div>
          </div>

          {this.state.data.has_viewpoint_location && (
            <div className="row">
              <div className="col-sm-6">
                {this.renderMap("viewpoint_lat", "viewpoint_long")}
                <div className="row pt-3">
                  <div className="col-sm-6">
                    {this.renderInput("viewpoint_lat", "Latitude", "number")}
                  </div>
                  <div className="col-sm-6">
                    {this.renderInput("viewpoint_long", "Longitude", "number")}
                  </div>
                </div>
              </div>

              <div className="col-sm-6">
                <div className="row">
                  <div className="col">
                    {this.renderPreview(
                      "viewpoint_image_aspect_ratio",
                      "viewpoint_filename"
                    )}
                  </div>
                </div>

                <div className="row mt-3">
                  <div className="col-sm-3">
                    {this.renderInput(
                      "viewpoint_image_aspect_ratio",
                      "Aspect Ratio",
                      "number",
                      "",
                      true
                    )}
                  </div>
                  <div className="col-sm-3">
                    {this.renderInput(
                      "viewpoint_image_height",
                      "Image Height",
                      "number"
                    )}
                  </div>
                  <div className="col-sm-3">
                    {this.renderInput(
                      "viewpoint_image_offset",
                      "Image Offset",
                      "number"
                    )}
                  </div>
                  <div className="col-sm-3">
                    {this.renderNumberInput(
                      "viewpoint_image_vertical_offset",
                      "Vertical Offset",
                      "number"
                    )}
                  </div>
                </div>

                <div className="row">
                  <div className="col-sm-12">
                    {this.renderFileInput("viewpoint_filename")}
                  </div>
                </div>
              </div>
            </div>
          )}

          <hr />

          <div className="row justify-content-between align-items-center">
            <div className="col">
              <Back onClick={this.handleBack} />
            </div>
            <div className="col text-end">
              {this.renderButton("Save", "btn btn-success rounded")}
            </div>
          </div>
        </form>
      </div>
    );
  }
}

export default PoiFrom;
