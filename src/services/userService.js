// Axios
import http from "./httpService";

// Constants
const apiEndpoint = "/users";

// Define and get API routes
function usersUrl(id) {
  return `${apiEndpoint}/${id}`;
}

function upgradeUrl(id) {
  return `${apiEndpoint}/upgrade/${id}`;
}

// API request to get all of the Users
export function getUsers() {
  return http.get(apiEndpoint);
}

// API request to get an User by ID
export function findUser(user_id) {
  return http.get(usersUrl(user_id));
}

// API request to register a new User
export function registerUsers(user) {
  return http.post(apiEndpoint, {
    name: user.name,
    email: user.email,
    password: user.password,
  });
}

// API request to create a new User or update an existing User by ID
export function saveUser(user) {
  if (user._id) {
    const body = { ...user };
    delete body._id;
    return http.put(usersUrl(user._id), body);
  }

  return http.post(apiEndpoint, user);
}

// API request to upgrade an existing User by ID
export function upgradeUser(user) {
  const body = { ...user };
  delete body._id;
  return http.put(upgradeUrl(user._id), body);
}

// API request to delete an existing User by ID
export function deleteUser(user_id) {
  return http.delete(usersUrl(user_id));
}

export default {
  getUsers,
  registerUsers,
  deleteUser,
  findUser,
  saveUser,
  upgradeUser,
};
