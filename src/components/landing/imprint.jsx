// Libraries
import React, { Component } from "react";

// Child Component
import Footer from "./footer";

class Imprint extends Component {
  render() {
    const url = window.location.search;

    if (url === "") {
      return (
        <React.Fragment>
          <div className="col page">
            <div className="row justify-content-between align-items-center">
              <div className="col-md-6 mt-2">
                <h2>Imprint</h2>
              </div>
              <div className="col-sm-12 col-md-5 col-lg-4 col-xl-3 mb-2 text-end"></div>
            </div>
            <hr className="mb-4" style={{ borderTop: "solid 3px black" }} />
            <div className="row justify-content-center mt-5">
              <div className="col-sm-6">
                <p className="font-weight-bold">
                  Technical realization (TU Dresden):
                </p>
                <p className="text-justify">
                  Prof. Dr. Stefan Gumhold;
                  <br />
                  Dipl. Inf. Benjamin Russig; David Groß, MSc. and Marzan Tasnim
                  Oyshi, MSc.
                </p>
                <p className="text-justify">
                  Development: David Victor Raj Anthony, Olena Horokh, Paul
                  Hunt, Tania Krisanty, Sneha Verma Prakash, Masoud Taghikhah,
                  Sadika Tanzila, Victor
                </p>
              </div>
              <div className="col-sm-6">
                <p>
                  <u>Contact</u>
                </p>
                <p className="text-justify">
                  Technische Universität Dresden, Professur für Computergraphik
                  und Visualisierung, 01062 Dresden <br />
                  Phone:{" "}
                  <a href="callto:+4935146338384">+49 (0)351 463 38384</a>
                  <br />
                  Email:{" "}
                  <a href="mailto:cgv-web@mailbox.tu-dresden.de">
                    cgv-web@mailbox.tu-dresden.de
                  </a>
                  <br />
                  Web:{" "}
                  <a href="https://tu-dresden.de/ing/informatik/smt/cgv">
                    https://tu-dresden.de/ing/informatik/smt/cgv
                  </a>
                </p>
              </div>
            </div>
            <hr style={{ borderTop: "dotted 1px silver" }} />
            <div className="row justify-content-center mt-4">
              <div className="col-sm-6">
                <p className="font-weight-bold">
                  Research (University Archive TU Dresden):
                </p>
                <p className="text-justify">
                  Dr. Matthias Lienert, Jutta Wiese
                </p>
              </div>
              <div className="col-sm-6">
                <p>
                  <u>Contact</u>
                </p>
                <p className="text-justify">
                  Technische Universität Dresden, Universitätsarchiv, 01062
                  Dresden <br />
                  Phone:{" "}
                  <a href="callto:+4935146334452">+49 (0)351 463 34452</a>
                  <br />
                  Email:{" "}
                  <a href="mailto:uniarchiv@tu-dresden.de">
                    uniarchiv@tu-dresden.de
                  </a>
                  <br />
                  Web:{" "}
                  <a href="https://tu-dresden.de/ua">
                    https://tu-dresden.de/ua
                  </a>
                </p>
              </div>
            </div>
            <hr style={{ borderTop: "dotted 1px silver" }} />
            <div className="row justify-content-center mt-4">
              <div className="col-sm-6">
                <p className="font-weight-bold">
                  Research, texts, editing (Stasi-Unterlagen-Archiv):
                </p>
                <p className="text-justify">
                  Cornelia Herold, M.A., Dr. Maria Fiebrandt, Luisa Fennert,
                  B.A.
                </p>
              </div>
              <div className="col-sm-6">
                <p>
                  <u>Contact</u>
                </p>
                <p className="text-justify">
                  Bundesarchiv/Stasi-Unterlagen-Archiv Dresden, Riesaer Straße
                  7, 01129 Dresden <br />
                  Phone: <a href="callto:+4930186653411">+49 (0)30 18 665-3411</a>
                  <br />
                  Email:{" "}
                  <a href="mailto:dresden.stasiunterlagenarchiv@bundesarchiv.de">
                    dresden.stasiunterlagenarchiv@bundesarchiv.de
                  </a>
                  <br />
                  Web:{" "}
                  <a href="www.stasi-unterlagen-archiv.de">
                    www.stasi-unterlagen-archiv.de
                  </a>
                </p>
              </div>
            </div>
          </div>

          <Footer />
        </React.Fragment>
      );
    } else {
      return (
        <React.Fragment>
          <div className="col page">
            <div className="row justify-content-between align-items-center">
              <div className="col-md-6 mt-2">
                <h2>Impressum</h2>
              </div>
              <div className="col-sm-12 col-md-5 col-lg-4 col-xl-3 mb-2 text-end"></div>
            </div>
            <hr className="mb-4" style={{ borderTop: "solid 3px black" }} />
            <div className="row justify-content-center mt-5">
              <div className="col-sm-6">
                <p className="font-weight-bold">
                  Technische Umsetzung (TU Dresden):
                </p>
                <p className="text-justify">
                  Prof. Dr. Stefan Gumhold;
                  <br />
                  Dipl. Inf. Benjamin Russig; David Groß, MSc. and Marzan Tasnim
                  Oyshi, MSc.
                </p>
                <p className="text-justify">
                  Entwicklung: David Victor Raj Anthony, Olena Horokh, Paul
                  Hunt, Tania Krisanty, Sneha Verma Prakash, Masoud Taghikhah,
                  Sadika Tanzila, Victor
                </p>
              </div>
              <div className="col-sm-6">
                <p>
                  <u>Kontakt</u>
                </p>
                <p className="text-justify">
                  Technische Universität Dresden, Professur für Computergraphik
                  und Visualisierung, 01062 Dresden <br />
                  Telefon:{" "}
                  <a href="callto:+4935146338384">+49 (0)351 463 38384</a>
                  <br />
                  E-mail:{" "}
                  <a href="mailto:cgv-web@mailbox.tu-dresden.de">
                    cgv-web@mailbox.tu-dresden.de
                  </a>
                  <br />
                  Web:{" "}
                  <a href="https://tu-dresden.de/ing/informatik/smt/cgv">
                    https://tu-dresden.de/ing/informatik/smt/cgv
                  </a>
                </p>
              </div>
            </div>
            <hr style={{ borderTop: "dotted 1px silver" }} />
            <div className="row justify-content-center mt-4">
              <div className="col-sm-6">
                <p className="font-weight-bold">
                  Recherche (Universitätsarchiv TU Dresden):
                </p>
                <p className="text-justify">
                  Dr. Matthias Lienert, Jutta Wiese
                </p>
              </div>
              <div className="col-sm-6">
                <p>
                  <u>Kontakt</u>
                </p>
                <p className="text-justify">
                  Technische Universität Dresden, Universitätsarchiv, 01062
                  Dresden <br />
                  Telefon:{" "}
                  <a href="callto:+4935146334452">+49 (0)351 463 34452</a>
                  <br />
                  E-mail:{" "}
                  <a href="mailto:uniarchiv@tu-dresden.de">
                    uniarchiv@tu-dresden.de
                  </a>
                  <br />
                  Web:{" "}
                  <a href="https://tu-dresden.de/ua">
                    https://tu-dresden.de/ua
                  </a>
                </p>
              </div>
            </div>
            <hr style={{ borderTop: "dotted 1px silver" }} />
            <div className="row justify-content-center mt-4">
              <div className="col-sm-6">
                <p className="font-weight-bold">
                  Recherche, Texte, Redaktion (Stasi-Unterlagen-Archiv):
                </p>
                <p className="text-justify">
                  Cornelia Herold, M.A., Dr. Maria Fiebrandt, Luisa Fennert,
                  B.A.
                </p>
              </div>
              <div className="col-sm-6">
                <p>
                  <u>Kontakt</u>
                </p>
                <p className="text-justify">
                  Bundesarchiv/Stasi-Unterlagen-Archiv Dresden, Riesaer Straße
                  7, 01129 Dresden <br />
                  Telefon: <a href="callto:+4935125080">+49 (0)351 2508 0</a>
                  <br />
                  E-mail:{" "}
                  <a href="mailto:dresden.stasi-unterlagen-archiv@bundesarchiv.de">
                    dresden.stasi-unterlagen-archiv@bundesarchiv.de
                  </a>
                  <br />
                  Web:{" "}
                  <a href="www.stasi-unterlagen-archiv.de">
                    www.stasi-unterlagen-archiv.de
                  </a>
                </p>
              </div>
            </div>
          </div>

          <Footer />
        </React.Fragment>
      );
    }
  }
}

export default Imprint;
