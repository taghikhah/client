// Libraries
import React from "react";

// Constants
const imageUrl = process.env.REACT_APP_IMAGE_URL;

const Thumbnail = ({ filename, alt, id }) => {
  const regex = /(http(s?)):\/\//i;
  let src = "";
  if (!filename) {
    src = `${process.env.PUBLIC_URL}/placeholder.png`;
  } else if (filename.match(regex)) {
    src = filename;
  } else {
    src = `${imageUrl}/${id}/${filename}`;
  }

  return (
    <div className="row align-items-center ">
      <div className="frame">
        <span className="helper"></span>
        <img src={src} alt={alt} key={alt} />
      </div>
    </div>
  );
};

export default Thumbnail;

/*
          style={{
            maxWidth: "200px",
            maxHeight: "200px",
            verticalAlign: "middle",
          }}
import React from "react";

const Thumbnail = ({ filename, alt }) => {
  return (
    <span style={{ maxHeight: "200px" }}>
      <img
        src={process.env.PUBLIC_URL + "/images/" + filename}
        alt={alt}
        style={{ maxWidth: "200px" }}
      />
    </span>
  );
};

export default Thumbnail;

*/
