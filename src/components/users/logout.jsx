// Libraries
import { Component } from "react";

// Functions
import authService from "../../services/authService";

class Logout extends Component {
  componentDidMount() {
    authService.logout();
    window.location = "/";
  }
  render() {
    return null;
  }
}

export default Logout;
